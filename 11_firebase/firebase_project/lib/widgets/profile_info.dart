import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ProfileInfo extends StatelessWidget {
  ProfileInfo(this.par, this.val);

  final String par;
  final String val;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Text(
              par,
              style: TextStyle(color: Colors.black, fontSize: 16),
              textAlign: TextAlign.start,
              textDirection: TextDirection.ltr,
            ),
          ),
          Expanded(
            child: Text(
              val,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
              textAlign: TextAlign.start,
              textDirection: TextDirection.ltr,
            ),
          ),
        ],
      ),
    );
  }
}
