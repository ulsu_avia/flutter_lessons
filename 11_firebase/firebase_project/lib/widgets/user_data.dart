import 'dart:async';
import 'dart:io' as io;
import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';

import 'package:firebase_project/components/login_form.dart';
import 'package:firebase_project/components/register_form.dart';
import 'package:firebase_project/components/auth_service.dart';

import 'package:firebase_project/views/home_view.dart';
import 'package:firebase_project/widgets/profile_info.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:ui' as ui;

class GetUserData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // CollectionReference users = FirebaseFirestore.instance.collection('users');
    //
    // return StreamBuilder<QuerySnapshot>(
    //   stream: users.snapshots(),
    //   builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    //     if (snapshot.hasError) {
    //       return Text('Something went wrong');
    //     }
    //
    //     if (snapshot.connectionState == ConnectionState.waiting) {
    //       return Text("Loading");
    //     }
    //
    //     return Column(
    //       children: snapshot.data.documents.map((DocumentSnapshot document) {
    //         return Container(child: Column(children: <Widget>[
    //           ProfileInfo('Имя:', document.data()['firstName']),
    //           ProfileInfo('Фамилия:', document.data()['lastName']),
    //           ProfileInfo('Телефон:', document.data()['phone']),
    //           ProfileInfo('Email:', FirebaseAuth.instance.currentUser.email),
    //         ],),);
    //       }).toList(),
    //     );
    //   },
    // );

    CollectionReference users = FirebaseFirestore.instance.collection('users');
    User user = FirebaseAuth.instance.currentUser;

    if(user.email == null)
      return Container();

    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(user.email).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Container(
            child: Column(
              children: <Widget>[
                ProfileInfo('Имя:', data['firstName']),
                ProfileInfo('Фамилия:', data['lastName']),
                ProfileInfo('Телефон:', data['phone']),
                ProfileInfo('Email:', FirebaseAuth.instance.currentUser.email),
              ],
            ),
          );
        }

        return Text("loading");
      },
    );
  }
}
