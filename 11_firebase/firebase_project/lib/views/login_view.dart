import 'dart:async';
import 'dart:io' as io;
import 'dart:io';

import 'package:firebase_project/components/verify.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';

import 'package:firebase_project/components/login_form.dart';
import 'package:firebase_project/components/register_form.dart';
import 'package:firebase_project/components/auth_service.dart';

import 'package:firebase_project/views/home_view.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:ui' as ui;

enum FormType { login, register }

class LoginView extends StatefulWidget {
  static const routeName = '/';

  LoginView({Key key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  FormType _formType = FormType.login;

  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  AuthService _authService = AuthService();
  User user;

  GlobalKey globalKey;

  @override
  void initState() {
    super.initState();
    globalKey = GlobalKey();
  }

  _switchForm() {
    setState(() {
      _formType =
          _formType == FormType.login ? FormType.register : FormType.login;
    });
  }

  @override
  Widget build(BuildContext context) {
    @override
    void initState() {
      // TODO: implement initState
      super.initState();
    }

    return FutureBuilder(
        // Initialize FlutterFire:
        future: _initialization,
        builder: (context, snapshot) {
          // Check for errors
          if (snapshot.hasError) {
            print("Error");
          }

          // Once complete, show your application
          if (snapshot.connectionState == ConnectionState.done) {
            FirebaseAuth.instance.authStateChanges().listen((User user) {
              if (user == null) {
                print('User is currently signed out!');
              } else {
                try {
                  FirebaseAuth.instance.currentUser.reload();
                } on FirebaseAuthException catch (e) {
                  print("Failed to reload user");
                  user = null;
                }
                print('User is signed in!');
                print(user.email);
                print(user.photoURL);
              }
            });

            if (FirebaseAuth.instance.currentUser == null) {
              return Scaffold(
                resizeToAvoidBottomPadding: false,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                ),
                body: SafeArea(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    _formType == FormType.login
                                        ? 'Вход'
                                        : 'Регистрация',
                                    style:
                                    Theme
                                        .of(context)
                                        .textTheme
                                        .headline4,
                                    textAlign: TextAlign.start,
                                  ),
                                ),
                                _formType == FormType.login
                                    ? LoginForm()
                                    : RegisterForm(),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                _formType != FormType.login
                                    ? 'Уже есть аккаунт?'
                                    : 'Еще нет аккаунта? ',
                              ),
                              FlatButton(
                                child: RichText(
                                  key: Key('enter'),
                                  text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: _formType != FormType.login
                                              ? 'Войти'
                                              : 'Зарегистрироваться',
                                        )
                                      ],
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .bodyText1),
                                ),
                                onPressed: _switchForm,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }else {
              if (FirebaseAuth.instance.currentUser.emailVerified == true)
                return HomePage();
              else
                return VerufyScreen(sendToEmail: '0');
            }

          }
          // Otherwise, show something whilst waiting for initialization to complete
          return Center(child: CircularProgressIndicator());
        });
  }
}
