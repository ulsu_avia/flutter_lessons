import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_project/views/user_profile.dart';

import 'package:firebase_project/views/home_view.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:image_pickers/image_pickers.dart';

class HomePage extends StatefulWidget {
  static const routeName = '/home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: <Widget>[
                  CircleAvatar(
                    radius: 65,
                    backgroundColor: Colors.white,
                    backgroundImage: NetworkImage(
                        'https://png.pngtree.com/png-vector/20190223/ourlarge/pngtree-test-line-black-icon-png-image_690888.jpg',
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              leading: Icon(Icons.account_circle_sharp),
              trailing: Icon(Icons.arrow_forward),
              title: Text("Ваш профиль"),
              onTap: () {
                //Navigator.of(context).pushNamed('/home');
                Navigator.of(context).pop('');
              },
              onLongPress: () {},
            ),
            ListTile(
              leading: Icon(Icons.notes),
              trailing: Icon(Icons.arrow_forward),
              title: Text("Заметки"),
              onTap: () {
                Navigator.of(context).pushNamed('/todo');
              },
              onLongPress: () {},
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Center(
          child: Container(
            padding: EdgeInsets.only(right: 45),
            child: Text('Ваш профиль'),
          ),
        ),
      ),
      body: Profile(),
    );
  }
}
