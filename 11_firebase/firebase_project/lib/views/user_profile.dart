import 'dart:async';
import 'dart:io' as io;
import 'dart:io';

import 'package:firebase_project/views/login_view.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';

import 'package:firebase_project/components/login_form.dart';
import 'package:firebase_project/components/register_form.dart';
import 'package:firebase_project/components/auth_service.dart';

import 'package:firebase_project/views/home_view.dart';
import 'package:firebase_project/widgets/profile_info.dart';
import 'package:firebase_project/widgets/user_data.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:ui' as ui;

class Profile extends StatefulWidget {
  static const routeName = '/userprofile';

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool isLoad = false;
  bool hasError = false;
  String errorMessage;
  String curImage ;

  AuthService _authService = AuthService();
  User user;

  GalleryMode _galleryMode = GalleryMode.image;
  GlobalKey globalKey;

  @override
  void initState() {
    getuser();
    super.initState();
    globalKey = GlobalKey();
  }

  getuser() {
    user = FirebaseAuth.instance.currentUser;
    print(user.uid);
  }

  List<Media> _listImagePaths = List();
  List<Media> _listVideoPaths = List();
  String dataImagePath = "";
  String urlImageApi;

  Future<void> selectImages() async {
    try {
      _galleryMode = GalleryMode.image;
      _listImagePaths = await ImagePickers.pickerPaths(
        galleryMode: _galleryMode,
        showGif: true,
        selectCount: 1,
        showCamera: true,
        cropConfig: CropConfig(enableCrop: true, height: 1, width: 1),
        compressSize: 500,
        uiConfig: UIConfig(
          uiThemeColor: Color(0xffff0000),
        ),
      );
      _listImagePaths.forEach((media) {
        print(media.path.toString());
        uploadFile(
          File(media.path),
        );
      });
      setState(() {});
    } on PlatformException {}
  }

  List<firebase_storage.UploadTask> _uploadTasks = [];

  /// The user selects a file, and the task is added to the list.
  Future<firebase_storage.UploadTask> uploadFile(File file) async {
    user = FirebaseAuth.instance.currentUser;
    firebase_storage.UploadTask uploadTask;

    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      if (file == null) {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("No file was selected")));
        return null;
      }

      // Create a Reference to the file
      firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance
          .ref()
          .child('users/${user.uid}/avatar.jpg');

      final metadata = firebase_storage.SettableMetadata(
          contentType: 'image/jpeg',
          customMetadata: {'picked-file-path': file.path});

      if (kIsWeb) {
        uploadTask = ref.putData(await file.readAsBytes(), metadata);
      } else {
        uploadTask = ref.putFile(io.File(file.path), metadata);
        curImage = file.path;
        uploadTask.whenComplete(() async => {
              ref.getDownloadURL().then((result) {
                setState(() {
                  if (result is String)
                    urlImageApi =
                        result.toString(); //use toString to convert as String
                });

                user.updateProfile(photoURL: urlImageApi).whenComplete(() => {
                      user.reload(),
                    });
              }),
            });
      }
    }
    return Future.value(uploadTask);
  }

  goBack(BuildContext context) {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Center(
            child: GestureDetector(
              onTap: () async {
                await selectImages();
              },
              child: CircleAvatar(
                radius: 65,
                backgroundColor: Colors.white,
                child: (curImage != null)
                    ? Image.file(
                        File(
                          curImage,
                        ),
                        fit: BoxFit.cover,
                      )
                    : (user.photoURL == null)
                        ? Icon(
                            Icons.photo,
                            size: 100.0,
                          )
                        : Image.network(user.photoURL),
              ),
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                GetUserData(),
                RaisedButton(
                  child: Text('Выйти'),
                  onPressed: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.of(context).pushReplacementNamed('/');
                  },
                  //onPressed: _handleSubmit,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
