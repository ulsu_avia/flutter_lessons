import 'dart:async';
import 'dart:io' as io;
import 'dart:io';
import 'dart:math';

import 'package:firebase_project/components/notes.dart';
import 'package:firebase_project/components/verify.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';

import 'package:firebase_project/components/login_form.dart';
import 'package:firebase_project/components/register_form.dart';
import 'package:firebase_project/components/auth_service.dart';
import 'package:firebase_project/components/notes.dart';

import 'package:firebase_project/views/home_view.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:ui' as ui;

class ToDo extends StatefulWidget {
  static const routeName = '/todo';

  ToDo({Key key}) : super(key: key);

  @override
  _ToDoState createState() => _ToDoState();
}

class _ToDoState extends State<ToDo> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  AuthService _authService = AuthService();

  Notes _notes = Notes();

  User user;

  GlobalKey globalKey;

  TextEditingController _controllerTitle = TextEditingController();
  TextEditingController _controllerDescription = TextEditingController();

  @override
  void initState() {
    super.initState();
    globalKey = GlobalKey();
  }

  @override
  Widget build(BuildContext context) {
    @override
    void initState() {
      // TODO: implement initState
      super.initState();
    }

    return FutureBuilder(
        // Initialize FlutterFire:
        future: _initialization,
        builder: (context, snapshot) {
          // Check for errors
          if (snapshot.hasError) {
            print("Error");
          }

          // Once complete, show your application
          if (snapshot.connectionState == ConnectionState.done) {
            FirebaseAuth.instance.authStateChanges().listen((User user) {
              if (user == null) {
                print('User is currently signed out!');
              } else {
                try {
                  FirebaseAuth.instance.currentUser.reload();
                } on FirebaseAuthException catch (e) {
                  print("Failed to reload user");
                  user = null;
                }
                print('User is signed in!');
              }
            });

            return Scaffold(
                appBar: AppBar(
                  title: Center(
                    child: Container(
                      padding: EdgeInsets.only(right: 45),
                      child: Text('Ваши заметки'),
                    ),
                  ),
                ),
                body: Column(
                  children: <Widget>[
                    Expanded(
                      child: StreamBuilder(
                        stream: FirebaseFirestore.instance
                            .collection('todo')
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          return ListView(
                            children: <Widget>[
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting)
                                Center(child: CircularProgressIndicator()),
                              if (snapshot.hasData)
                                ...snapshot.data.documents
                                    .map((todo) => Dismissible(
                                          // Each Dismissible must contain a Key. Keys allow Flutter to
                                          // uniquely identify widgets.
                                          key: Key(todo.id),
                                          // Provide a function that tells the app
                                          // what to do after an item has been swiped away.
                                          onDismissed: (direction) {
                                            try {
                                              todo.reference.delete();
                                            } catch (e) {
                                              print(e);
                                            }

                                            // Show a snackbar. This snackbar could also contain "Undo" actions.
                                            Scaffold.of(context).showSnackBar(
                                                SnackBar(
                                                    content: Text(
                                                        "Заметка ${todo['title']} удалена")));
                                          },

                                          child: GestureDetector(
                                            onLongPress: () {
                                              _controllerTitle.text =
                                                  todo['title'];
                                              _controllerDescription.text =
                                                  todo['description'];

                                              _notes.editDataDialog(
                                                context: context,
                                                documentSnapshot: todo,
                                                edit: true,
                                                controllerTitle:
                                                    _controllerTitle,
                                                controllerDescription:
                                                    _controllerDescription,
                                              );
                                            },
                                            child: CheckboxListTile(
                                              title: Text(todo['title']),
                                              value: todo['isDone'],
                                              onChanged: (bool value) async {
                                                //setState(()  {
                                                await _notes.UpdateStatusData(
                                                    todo);
                                                //});
                                              },
                                              secondary:
                                                  const Icon(Icons.notes),

                                              // title: Text(todo['title']),
                                              // trailing: Text('${todo['isDone']}'),
                                            ),
                                          ),
                                        ))
                                    .toList(),
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                ), // This trailing comma makes auto-formatting nicer for build methods.
                floatingActionButtonLocation:
                    FloatingActionButtonLocation.centerDocked,
                floatingActionButton: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FloatingActionButton(
                        onPressed: () {
                          _controllerTitle.text = '';
                          _controllerDescription.text = '';
                          _notes.editDataDialog(
                              context: context,
                              documentSnapshot: null,
                              edit: false,
                            controllerTitle:
                            _controllerTitle,
                            controllerDescription:
                            _controllerDescription,);
                        },
                        child: Icon(Icons.add),
                      ),
                      FloatingActionButton(
                        onPressed: () {
                          _notes.editFirstData(context,
                            _controllerTitle,
                            _controllerDescription,);
                        },
                        child: Icon(Icons.edit),
                      ),
                      FloatingActionButton(
                        onPressed: () {
                          _notes.deleteData(context);
                        },
                        child: Icon(Icons.delete),
                      )
                    ],
                  ),
                ));
          }
          // Otherwise, show something whilst waiting for initialization to complete
          return Center(child: CircularProgressIndicator());
        });
  }
}
