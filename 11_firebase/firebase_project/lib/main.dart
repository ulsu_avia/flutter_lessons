import 'package:firebase_project/views/home_view.dart';
import 'package:firebase_project/views/todo.dart';
import 'package:firebase_project/views/user_profile.dart';
import 'package:firebase_project/components/verify.dart';
import 'package:flutter/material.dart';
import 'package:firebase_project/views/login_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),

      initialRoute: '/',

      onUnknownRoute: (RouteSettings settings) {
        return MaterialPageRoute(builder: (BuildContext context) {
          return NotFound();
        });
      },
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case LoginView.routeName:
            return MaterialPageRoute(builder: (BuildContext context) {
              return LoginView();
            });
          case Profile.routeName:
            return MaterialPageRoute(builder: (BuildContext context) {
              return Profile();
            });
          case ToDo.routeName:
            return MaterialPageRoute(builder: (BuildContext context) {
              return ToDo();
            });
          case HomePage.routeName:
            return MaterialPageRoute(builder: (BuildContext context) {
              return HomePage();
            });
          case VerufyScreen.routeName:
            final args = settings.arguments as Map<String, dynamic>;

            if (args != null && args.containsKey('sendToEmail')) {
              return MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (BuildContext context) {
                    return VerufyScreen(sendToEmail: args['sendToEmail']);
                  });
            }

            return MaterialPageRoute(builder: (BuildContext context) {
              return VerufyScreen();
            });
          default:
            return MaterialPageRoute(builder: (BuildContext context) {
              return NotFound();
            });
        }
      },


      // routes: {
      //   '/' : (BuildContext context) => LoginView(),
      //   '/userprofile' : (BuildContext context) => Profile(),
      //   '/home' : (BuildContext context) => HomePage(),
      //   '/verify' : (BuildContext context) => VerufyScreen(),
      // },
    );
  }
}


class NotFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Page not found'),
      ),
    );
  }
}