import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AuthService
{

  //final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<Map<int, dynamic>> signUp(String email, String password) async{

    Map<int, dynamic> returnMap;
    String errorMessage;

    User user;

    errorMessage= '';

    try {
      UserCredential result = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email,
          password: password
      );

      user = result.user;

    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        errorMessage = 'Слишком слабый пароль';
        print(errorMessage);
      } else if (e.code == 'email-already-in-use') {
        errorMessage = 'Пользователь с таким email уже существует';
        print(errorMessage);
      }
    } catch (e) {
      print(e);
    }

    returnMap = {
      1: errorMessage,
      2: user,
    };
    return returnMap;

  }


  Future<Map<int, dynamic>> signIn(String email, String password) async{

    Map<int, dynamic> returnMap;
    String errorMessage;

    User user;

    errorMessage= '';

    try {
      UserCredential result = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: email,
          password: password
      );

      user = result.user;


    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        errorMessage = 'Пользователь не найден';
        print(errorMessage);
      } else if (e.code == 'wrong-password') {
        errorMessage = 'Не верный пароль';
        print(errorMessage);
      }
    }
    // catch (e) {
    //     print(e);
    // }

    returnMap = {
      1: errorMessage,
      2: user,
    };
    return returnMap;
  }

  Future<User> getCurrentUser() async {
    User user = await FirebaseAuth.instance.currentUser;
    return user;
  }

  Future<User> updateUser(String updateURL) async {
    print(updateURL);
    User user = await FirebaseAuth.instance.currentUser;
    user.updateProfile(photoURL: updateURL);
    user.reload();
    print(user.photoURL);
    User updatedser = await FirebaseAuth.instance.currentUser;
    print(updatedser.photoURL);
    return updatedser;
  }

  Future<void> signOut() {
    return FirebaseAuth.instance.signOut();

  }


  void AddUser (firstName, lastName, phoneNum) {

    User user = FirebaseAuth.instance.currentUser;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  try {
    // Call the user's CollectionReference to add a new user
    users.doc(user.email).set({
      'firstName': firstName, // John Doe
      'lastName': lastName, // Stokes and Sons
      'phone': phoneNum // 42
    });
  }
  on FirebaseAuthException catch (e) {
       print("Failed to add user: $e");
  }

  }
}