import 'dart:async';
import 'dart:io' as io;
import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:firebase_project/components/login_form.dart';
import 'package:firebase_project/components/register_form.dart';
import 'package:firebase_project/components/auth_service.dart';
import 'package:firebase_project/components/verify.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:ui' as ui;

import 'package:firebase_project/utils/validate_email.dart';

class LoginForm extends StatefulWidget {
  LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool successMessage = false;

  String errorMessage = '';

  AuthService _authService = AuthService();

  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  User user;

  bool _isSuccess = false;
  void _handleSubmit() {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isSuccess = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    getuser() async {
      user = await _authService.getCurrentUser();
    }

    @override
    void initState() {
      // TODO: implement initState
      super.initState();
      getuser();
    }

    void _onSignIn() async {
      if (_formKey.currentState.validate()) {
        //print('123');
        _formKey.currentState.save();
        Map<int, dynamic> returnMap = await _authService
            .signIn(_controllerEmail.text, _controllerPassword.text)
            .then((value) {

          setState(() {
            errorMessage = value[1];

            if (value[2] != null) {
              user = value[2];
            }
          });

          if (value[1].isEmpty && value[2] != null) {
            {
              //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => VerufyScreen(false)));
              Navigator.of(context).pushNamed('/verify', arguments: {
                'sendToEmail': '0',
              });
            }
          }
        });

        _formKey.currentState.reset();
      }
    }

    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            key: Key('fieldEmail'),
            controller: _controllerEmail,
            validator: (value) {
              if (value == '') return 'Введите email';
              if (!validateEmail(value))
                return 'Поле email заполнено не корректно';
              return null;
            },
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(labelText: 'Email'),
          ),
          TextFormField(
            key: Key('fieldPhone'),
            controller: _controllerPassword,
            enableSuggestions: false,
            obscureText: true,
            autocorrect: false,
            validator: (value) {
              if (value == '') return 'Введите пароль';
              return null;
            },
            decoration: InputDecoration(labelText: 'Пароль'),
          ),
          if (errorMessage.isNotEmpty)
            Padding(
                padding: const EdgeInsets.all(3.0),
                child: Text(errorMessage,
                    style: TextStyle(color: Colors.red),
                    textDirection: TextDirection.ltr)),
          RaisedButton(
            child: Text('Войти'),
            onPressed: () {
              _onSignIn();
            },
            //   () {
            // if (_formKey.currentState.validate()) {
            //   _formKey.currentState.save();
            //   setState(() {
            //     successMessage = true;
            //   });
            // }
            //},
          ),
        ],
      ),
    );
  }
}
