import 'dart:async';
import 'dart:io' as io;
import 'dart:io';

import 'package:firebase_project/utils/validate_phone.dart';
import 'package:firebase_project/views/home_view.dart';
import 'package:firebase_project/views/login_view.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:firebase_project/components/login_form.dart';
import 'package:firebase_project/components/register_form.dart';
import 'package:firebase_project/components/auth_service.dart';
import 'package:firebase_project/main.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:ui' as ui;

import 'package:firebase_project/utils/validate_email.dart';

class VerufyScreen extends StatefulWidget {
  static const routeName = '/verify';

  VerufyScreen({Key key, this.sendToEmail}) : super(key: key);

  final String sendToEmail;

  @override
  _VerufyScreenState createState() => _VerufyScreenState();
}

class _VerufyScreenState extends State<VerufyScreen> {
  AuthService _authService = AuthService();
  Timer timer;
  User user;
  bool viewVerifyMessage = false;

  checkEmail() {
    user = FirebaseAuth.instance.currentUser;
    if (widget.sendToEmail == '1') {
      user.sendEmailVerification();
    } else {
      checkEmailVerifiedOnLogin();
    }

    print(user.email);
    timer = Timer.periodic(Duration(seconds: 5), (timer) {
      checkEmailVerified();
    });
  }

  @override
  void initState() {
    checkEmail();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (user != null && viewVerifyMessage) {
      return Scaffold(
        body: Center(
          child: Text(
            'Ссылка на подтверждение аккаунта отправлена Вам на почту ${user.email}',
            style: TextStyle(color: Colors.green),
            textAlign: TextAlign.center,
          ),
        ),
      );
    }

    return Center(child: CircularProgressIndicator());
  }

  Future<void> checkEmailVerified() async {
    user = await _authService.getCurrentUser();
    await user.reload();
    if (user.emailVerified) {
      timer.cancel();
      Navigator.of(context).pushReplacementNamed('/home');
    } else {
      setState(() {
        viewVerifyMessage = true;
      });
    }
  }

  Future<void> checkEmailVerifiedOnLogin() async {
    user = await FirebaseAuth.instance.currentUser;
    await user.reload();
    if (user.emailVerified) {
      Navigator.of(context).pushReplacementNamed('/home');
    } else {
      setState(() {
        viewVerifyMessage = true;
      });
    }
  }
}
