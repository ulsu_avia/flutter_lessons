import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Notes {
  Future<void> addData(TextEditingController controllerTitle,
      TextEditingController controllerDescription) {
    CollectionReference data = FirebaseFirestore.instance.collection('todo');
    // Call the user's CollectionReference to add a new user
    return data
        .add({
          'title': controllerTitle.text,
          'description': controllerDescription.text,
          'isDone': false,
        })
        .then((value) => print("Data Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> UpdateStatusData(
      QueryDocumentSnapshot queryDocumentSnapshot) async {
    try {
      queryDocumentSnapshot['isDone'] == false
          ? queryDocumentSnapshot.reference.update({'isDone': true})
          : queryDocumentSnapshot.reference.update({'isDone': false});
    } catch (e) {
      print(e);
    }
  }

  Future<void> UpdateData(
      QueryDocumentSnapshot queryDocumentSnapshot,
      TextEditingController controllerTitle,
      TextEditingController controllerDescription) async {
    try {
      queryDocumentSnapshot.reference.update({'title': controllerTitle.text});
      queryDocumentSnapshot.reference
          .update({'description': controllerDescription.text});
    } catch (e) {
      print(e);
    }
  }

  Future<void> deleteData(BuildContext context) async {
    try {
      bool deleted = false;
      FirebaseFirestore.instance
          .collection('todo')
          .getDocuments()
          .then((snapshot) {
        for (DocumentSnapshot ds in snapshot.documents) {
          if (ds.data()["isDone"] == true) {
            ds.reference.delete();
            deleted = true;
          }
        }

        if (!deleted) {
          showDialog(
              context: context,
              builder: (context) {
                Future.delayed(Duration(seconds: 1), () {
                  Navigator.of(context).pop(true);
                });
                return AlertDialog(
                  title: Text(
                    'Выберите заметки!',
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                );
              });
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> editFirstData(
      BuildContext context,
      TextEditingController controllerTitle,
      TextEditingController controllerDescription) async {
    bool edited = false;

    try {
      FirebaseFirestore.instance
          .collection('todo')
          .getDocuments()
          .then((snapshot) {
        for (QueryDocumentSnapshot ds in snapshot.documents) {
          if (ds.data()["isDone"] == true) {
            controllerTitle.text = ds.data()["title"];
            controllerDescription.text = ds.data()['description'];
            editDataDialog(
              context: context,
              documentSnapshot: ds,
              edit: true,
              controllerTitle: controllerTitle,
              controllerDescription: controllerDescription,
            );
            edited = true;
            return;
          }
        }

        if (!edited) {
          showDialog(
              context: context,
              builder: (context) {
                try {
                  Future.delayed(Duration(seconds: 1), () {
                    Navigator.of(context).pop(true);
                  });
                } catch (e) {
                  print(e);
                }
                return AlertDialog(
                  title: Text(
                    'Выберите заметку!',
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                );
              });
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void editDataDialog(
      {BuildContext context,
      QueryDocumentSnapshot documentSnapshot,
      bool edit,
      TextEditingController controllerTitle,
      TextEditingController controllerDescription}) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            // title: Text(
            //   'Заметка',
            //   style: TextStyle(color: Colors.black, fontSize: 16),
            //   textAlign: TextAlign.center,
            //   //textDirection: TextDirection.,
            // ),
            content: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(5),
                  child: TextFormField(
                    key: Key('fieldTitle'),
                    controller: controllerTitle,
                    validator: (value) {
                      if (value == '') return 'Введите заголовок заметки';
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: 'Заголовок заметки',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(left: 5, right: 5),
                    child: TextFormField(
                      key: Key('fieldDescription'),
                      keyboardType: TextInputType.multiline,
                      minLines: 40,
                      maxLines: null,
                      controller: controllerDescription,
                      validator: (value) {
                        if (value == '') return 'Введите текст заметки';
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: 'Текст заметки',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                    ),
                  ),
                ),
                // MaterialButton(
                //     elevation: 5.0, child: Text('Принять'), onPressed: () {}),
                Container(
                  padding: EdgeInsets.only(left: 5, right: 5, bottom: 10),
                  child: SizedBox(
                    child: RaisedButton(
                      child: Text('Сохранить'),
                      onPressed: () {
                        if (!edit) {
                          addData(controllerTitle, controllerDescription);
                        } else {
                          UpdateData(documentSnapshot, controllerTitle,
                              controllerDescription);
                        }
                        Navigator.of(context).pop('');
                      },
                    ),
                    width: double.infinity,
                  ),
                )
              ],
            ),
          );
        });
  }
}
