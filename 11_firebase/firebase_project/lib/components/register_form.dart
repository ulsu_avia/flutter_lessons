import 'dart:async';
import 'dart:io' as io;
import 'dart:io';

import 'package:firebase_project/utils/validate_phone.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:firebase_project/components/login_form.dart';
import 'package:firebase_project/components/register_form.dart';
import 'package:firebase_project/components/auth_service.dart';
import 'package:firebase_project/components/verify.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:ui' as ui;

import 'package:firebase_project/utils/validate_email.dart';

class RegisterForm extends StatefulWidget {
  RegisterForm({Key key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  AuthService _authService = AuthService();

  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();
  TextEditingController _controllerPasswordRepeat = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();
  TextEditingController _controllerFirstName = TextEditingController();
  TextEditingController _controllerLastName = TextEditingController();

  String errorMessage = '';

  User user;

  bool _isSuccess = false;
  void _handleSubmit() {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isSuccess = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    getuser() async {
      user = await _authService.getCurrentUser();
    }

    @override
    void initState() {
      // TODO: implement initState
      super.initState();
      getuser();
    }

    void _onSubmit() async {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        if (_controllerPassword.text == _controllerPasswordRepeat.text) {
          Map<int, dynamic> returnMap = await _authService
              .signUp(_controllerEmail.text, _controllerPassword.text)
              .then((value) {

            setState(() {
              errorMessage = value[1];

              if (value[2] != null) {
                user = value[2];
              }
            });

            if (value[1].isEmpty && value[2] != null) {
              try {
                _authService.AddUser(_controllerFirstName.text,
                    _controllerLastName.text, _controllerPhone.text);
              } on FirebaseAuthException catch (e) {
                print("Failed to write user data");
              }
              //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => VerufyScreen(true)));
              Navigator.of(context).pushNamed('/verify', arguments: {
                'sendToEmail': '1',
              });
            }
          });
        }
      }
    }

    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            key: Key('fieldFirstName'),
            controller: _controllerFirstName,
            decoration: InputDecoration(labelText: 'Имя'),
            validator: (value) {
              if (value == '') return 'Введите имя';
              return null;
            },
          ),
          TextFormField(
            key: Key('fieldLastName'),
            controller: _controllerLastName,
            decoration: InputDecoration(labelText: 'Фамилия'),
            validator: (value) {
              if (value == '') return 'Введите фамилию';
              return null;
            },
          ),
          TextFormField(
            key: Key('fieldPhone'),
            maxLength: 11,
            controller: _controllerPhone,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly
            ],
            decoration: InputDecoration(labelText: 'Телефон'),
            validator: (value) {
              if (value == '') return 'Заполните поле телефон';
              if (!validatePhone(value)) return 'Телефон не корректный';
              return null;
            },
          ),
          TextFormField(
            key: Key('fieldEmail'),
            controller: _controllerEmail,
            decoration: InputDecoration(labelText: 'Email'),
            validator: (value) {
              if (value == '') return 'Заполните поле email';
              if (!validateEmail(value)) return 'Email не корректный';
              return null;
            },
          ),
          TextFormField(
            key: Key('fieldPassword'),
            controller: _controllerPassword,
            enableSuggestions: false,
            obscureText: true,
            autocorrect: false,
            decoration: InputDecoration(labelText: 'Пароль'),
            validator: (value) {
              if (value == '') return 'Заполните пароль';
              if (value != _controllerPasswordRepeat.text)
                return 'Пароли не совпадают';
              return null;
            },
          ),
          TextFormField(
            key: Key('fieldPasswordRepeat'),
            controller: _controllerPasswordRepeat,
            enableSuggestions: false,
            obscureText: true,
            autocorrect: false,
            decoration: InputDecoration(labelText: 'Повторите пароль'),
            validator: (value) {
              if (value == '') return 'Повторите пароль';
              if (value != _controllerPassword.text)
                return 'Пароли не совпадают';
              return null;
            },
          ),
          if (errorMessage.isNotEmpty)
            Padding(
                padding: const EdgeInsets.all(3.0),
                child: Text(errorMessage,
                    style: TextStyle(color: Colors.red),
                    textDirection: TextDirection.ltr)),
          RaisedButton(
            child: Text('Отправить'),
            onPressed: _onSubmit,
            //onPressed: _handleSubmit,
          ),
          //if (_isSuccess) Text('Вы успешно зарегистрировались')
        ],
      ),
    );
  }
}
