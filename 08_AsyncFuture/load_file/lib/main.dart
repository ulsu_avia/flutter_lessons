import 'package:flutter/material.dart';
import 'fetch_file.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Load file'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final myController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  String _text = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: 2,
                    child: Container(
                      //width: double.infinity,
                      //width: double.maxFinite,
                      padding:
                          const EdgeInsets.only(top: 10, left: 10, bottom: 10),
                      child: Theme(
                        data: ThemeData(
                          primaryColor: Colors.black,
                          primaryColorDark: Colors.black,
                        ),
                        child: TextField(
                          controller: myController,
                          decoration: new InputDecoration(
                            contentPadding:
                                EdgeInsets.only(left: 20, top: 20, bottom: 20),
                            border: new OutlineInputBorder(
                                borderRadius: const BorderRadius.only(
                                  bottomLeft: const Radius.circular(10.0),
                                  topLeft: const Radius.circular(10.0),
                                ),
                                borderSide: new BorderSide(
                                    color: Colors.black, width: 2)),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.only(
                                  bottomLeft: const Radius.circular(10.0),
                                  topLeft: const Radius.circular(10.0),
                                ),
                                borderSide: new BorderSide(
                                    color: Colors.black, width: 2)),
                            hintText: 'Введите значение',
                            //helperText: 'Поле для поиска заметок',
                            labelText: 'Поиск',
                            labelStyle: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Container(
                      height: 80,
                      //width: 100,
                      padding:
                          const EdgeInsets.only(top: 10, right: 10, bottom: 10),
                      child: RaisedButton(
                        onPressed: () {
                          setState(
                            () {
                              _text = myController.text;
                            },
                          );
                          //print("2222");
                          //FocusScope.of(context).requestFocus(nodeTwo);
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: const BorderRadius.only(
                            bottomRight: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0),
                          ),
                        ),
                        color: Colors.black,
                        child: Text(
                          "Найти",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: GetDataFromFile(
                data: _text,
              ),
            ),
            //Text("${_text}")
          ],
        ),
      ),
    );
  }
}

class GetDataFromFile extends StatefulWidget {
  GetDataFromFile({Key key, this.data}) : super(key: key);

  String data = "";

  @override
  _GetDataFromFileState createState() => _GetDataFromFileState();
}

class _GetDataFromFileState extends State<GetDataFromFile> {
  @override
  Widget build(BuildContext context) {
    if (widget.data.length > 0) {
      return Container(
        child: FutureBuilder(
          future: fetchFileFromAssets('assets/${widget.data}'),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.active:
                return Center(
                  child: CircularProgressIndicator(),
                );
              case ConnectionState.waiting:
                return Center(
                  child: CircularProgressIndicator(),
                );
              case ConnectionState.done:
                try {
                  return SafeArea(child: SingleChildScrollView(
                    child: Text(
                      snapshot.data,
                      style: TextStyle(color: Colors.black, fontSize: 15),
                      textAlign: TextAlign.justify,
                      textDirection: TextDirection.ltr,
                    ),
                  ));
                } catch (e) {
                  print(e.toString());
                }
                return SingleChildScrollView(
                  child: SafeArea(child: Text("Не удалось загрузить файл")),
                );
              default:
                return Center(
                  child: CircularProgressIndicator(),
                );
            }
          },
        ),
      );
    } else {
      return Container();
    }
  }
}
