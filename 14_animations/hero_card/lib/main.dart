import 'package:flutter/material.dart';
import 'package:hero_card/space.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeView(title: 'Animations'),
    );
  }
}

class HomeView extends StatefulWidget {
  HomeView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black54,
        title: Align(alignment: Alignment.center, child: Text(widget.title)),
      ),
      body: ListView(
        children: spaces
            .map((spaces) => GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => DetailView(data: spaces, title: widget.title,),
                      ),
                    );
                  },
                  child: Stack(
                    overflow: Overflow.visible,
                    children: [
                      Hero(
                        tag: spaces.id,
                        child: Material(
                          child: Container(
                            child: Card(
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(20),
                                ),
                              ),
                              child: Column(
                                children: [
                                  // Expanded(
                                  //   flex: 2,
                                  //   child:
                                  Container(
                                    height: 80,
                                    width: double.infinity,
                                    child: Image.asset(
                                      spaces.image,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  //),
                                  // Expanded(
                                  //   child:
                                  Container(
                                    color: Colors.grey,
                                    alignment: Alignment.centerLeft,
                                    height: 60,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        '${spaces.description.substring(0, 40)}...',
                                      ),
                                    ),
                                  ),
                                  //),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 40,
                        right: 20,
                        child: Hero(
                          tag: '${spaces.id}-button',
                          child: Material(
                            child: Container(
                              padding: EdgeInsets.all(10),
                              color: Colors.yellow,
                              child: Icon(Icons.add),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ))
            .toList(),
      ),
    );
  }
}

class DetailView extends StatelessWidget {
  const DetailView({Key key, this.title, this.data}) : super(key: key);

  final Space data;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black54,
        title: Align(alignment: Alignment.center, child: Text(title)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 30),
              child: Stack(
                overflow: Overflow.visible,
                children: [
                  Hero(
                    tag: data.id,
                    child: Image.asset(data.image),
                  ),
                  Positioned(
                    bottom: -20,
                    right: 20,
                    child: Hero(
                      tag: '${data.id}-button',
                      child: Material(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            color: Colors.yellow,
                            child: Icon(Icons.remove),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Hero(
              tag: '${data.id}-title',
              child: Text(
                data.description,
                style: TextStyle(color: Colors.black, fontSize: 15),
                textAlign: TextAlign.justify,
                textDirection: TextDirection.ltr,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
