part of 'breeds_bloc.dart';

abstract class BreedsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class BreedsFetched extends BreedsEvent {}
