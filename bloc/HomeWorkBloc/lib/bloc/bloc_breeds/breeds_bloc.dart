// Dart imports:
import 'dart:async';

// Package imports:
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

// Project imports:
import 'package:HomeWorkBloc/bloc/connect_cubit/connect_cubit.dart';
import 'package:HomeWorkBloc/model/git_hub_jobs.dart';

part 'breeds_event.dart';
part 'breeds_state.dart';

const _breedsLimit = 50;


class BreedsBloc extends Bloc<BreedsEvent, BreedsState> {
  BreedsBloc({@required this.dioClient, this.connetCubit}) : super(const BreedsState())
  {
    _connetCubitSubscription = connetCubit.listen((state) {
        _connectionStatus = state;
    });
  }


  final Dio dioClient;


  final ConnetCubit connetCubit;
  StreamSubscription _connetCubitSubscription;
  ConnectionStatus _connectionStatus = ConnectionStatus.connected;

  @override
  Stream<Transition<BreedsEvent, BreedsState>> transformEvents(
    Stream<BreedsEvent> events,
    TransitionFunction<BreedsEvent, BreedsState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<BreedsState> mapEventToState(BreedsEvent event) async* {
    if (event is BreedsFetched) {
      yield await _mapBreedsFetchedToState(state);
    }
  }

  Future<BreedsState> _mapBreedsFetchedToState(BreedsState state) async {
    try {

      connetCubit.checkConnectivity();

      if(_connectionStatus == ConnectionStatus.connected) {
        if (state.status == BreedsStatus.initial) {
          final Map breeds = await _fetchPosts();
          return state.copyWith(
            status: BreedsStatus.success,
            breeds: changeDogSubBreedsListToString(breeds),
          );
        }
        final posts = await _fetchPosts(state.breeds.length);
        return posts.isEmpty
            ? state.copyWith(status: BreedsStatus.failure)
            : state.copyWith(
          status: BreedsStatus.success,
          breeds: Map.of(state.breeds)
            ..addAll(changeDogSubBreedsListToString(posts)),
        );
       }else{
         return state.copyWith(status: BreedsStatus.failure);
       }
    } on Exception {
      return state.copyWith(status: BreedsStatus.failure);
    }
  }

  Future<Map> _fetchPosts([int startIndex = 0]) async {

    try {
      final response = await dioClient.get(
        'https://dog.ceo/api/breeds/list/all',
      );
      if (response.statusCode == 200) {
        final Breeds _breeds = Breeds.fromJson(response.data);
        assert(_breeds.status == 'success');
        return _breeds.message;
      }
    } on DioError {
      throw Exception('error fetching posts');
    }

  }

  Map changeDogSubBreedsListToString(Map posts) {

    posts.forEach((key, value) {
        if(value.length>0)
        {
          posts.update(key, (value) => value.join(",").toString());
        }else
        {
          posts.update(key, (value) => null);
        }
      });

    return posts;
  }

  bool _hasReachedMax(int postsCount) => postsCount < _breedsLimit ? false : true;


  @override
  Future<void> close() {
    _connetCubitSubscription.cancel();
    return super.close();
  }

}
