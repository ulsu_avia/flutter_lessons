part of 'breeds_bloc.dart';

enum BreedsStatus { initial, success, failure }

class BreedsState extends Equatable {
  const BreedsState({
    this.status = BreedsStatus.initial,
    this.breeds = const {},
  });

  final BreedsStatus status;
  final Map breeds;

  BreedsState copyWith({
    BreedsStatus status,
    Map breeds,
    bool hasReachedMax,
  }) {
    return BreedsState(
      status: status ?? this.status,
      breeds: breeds ?? this.breeds,
    );
  }

  @override
  List<Object> get props => [status, breeds];
}
