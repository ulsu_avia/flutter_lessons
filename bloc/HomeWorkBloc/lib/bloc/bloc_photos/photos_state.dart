part of 'photos_bloc.dart';

enum PhotosStatus { initial, updated, success, failure }

class PhotosState extends Equatable {
  const PhotosState({
    this.status = PhotosStatus.initial,
    this.breed = '',
    this.photos = const [],
  });

  final PhotosStatus status;
  final String breed;
  final List photos;

  PhotosState copyWith({
    PhotosStatus status,
    String breed,
    List photos,
  }) {
    return PhotosState(
      status: status ?? this.status,
      breed: breed ?? this.breed,
      photos: photos ?? this.photos,
    );
  }

  @override
  List<Object> get props => [status, breed, photos];
}
