part of 'photos_bloc.dart';

abstract class PhotosEvent extends Equatable {
  const PhotosEvent();

  @override
  List<Object> get props => [];
}

class PhotosFetched extends PhotosEvent {
  const PhotosFetched(this.breed);

  final String breed;

  @override
  List<Object> get props => [breed];
}


class PhotosUpdate extends PhotosEvent {}
