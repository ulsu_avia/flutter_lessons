// Dart imports:
import 'dart:async';

// Package imports:
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

// Project imports:
import 'package:HomeWorkBloc/bloc/connect_cubit/connect_cubit.dart';
import 'package:HomeWorkBloc/model/git_hub_jobs.dart';

part 'photos_event.dart';
part 'photos_state.dart';

class PhotosBloc extends Bloc<PhotosEvent, PhotosState> {
  PhotosBloc({@required this.dioClient, this.connetCubit})
      : super(const PhotosState()) {
    _connetCubitSubscription = connetCubit.listen((state) {
      _connectionStatus = state;
    });
  }

  final Dio dioClient;

  final ConnetCubit connetCubit;
  StreamSubscription _connetCubitSubscription;
  ConnectionStatus _connectionStatus = ConnectionStatus.connected;

  @override
  Stream<PhotosState> mapEventToState(PhotosEvent event) async* {
    if (event is PhotosFetched) {
      yield await state.copyWith(status: PhotosStatus.updated, photos: []);
      yield await _mapPhotosFetchedToState(event, state);
    } else if (event is PhotosUpdate) {
      yield await _mapPhotosUpdateToState(event, state);
    }
  }

  Future<PhotosState> _mapPhotosFetchedToState(
      PhotosFetched event, PhotosState state) async {
    try {
      connetCubit.checkConnectivity();

      if (_connectionStatus == ConnectionStatus.connected) {
        final photos = await _fetchPhotos(event.breed);
        return photos.isEmpty
            ? state.copyWith(
                status: PhotosStatus.failure,
                breed: event.breed,
                photos: photos)
            : state.copyWith(
                status: PhotosStatus.success,
                breed: event.breed,
                photos: photos,
              );
      } else {
        return state.copyWith(status: PhotosStatus.failure);
      }
    } on Exception {
      return state.copyWith(status: PhotosStatus.failure);
    }
  }

  Future<PhotosState> _mapPhotosUpdateToState(
      PhotosUpdate event, PhotosState state) async {
    try {
      connetCubit.checkConnectivity();

      if (_connectionStatus == ConnectionStatus.connected) {
        final photos = await _fetchPhotos(state.breed);
        return photos.isEmpty
            ? state.copyWith(
                status: PhotosStatus.failure,
                breed: state.breed,
                photos: photos)
            : state.copyWith(
                status: PhotosStatus.success,
                breed: state.breed,
                photos: photos,
              );
      } else {
        return state.copyWith(status: PhotosStatus.failure);
      }
    } on Exception {
      return state.copyWith(status: PhotosStatus.failure);
    }
  }

  Future<List> _fetchPhotos(String breed) async {
    final response = await dioClient.get(
      'https://dog.ceo/api/breed/${breed}/images/random/10',
    );
    if (response.statusCode == 200) {
      final Photos _photos = Photos.fromJson(response.data);
      assert(_photos.status == 'success');
      return _photos.message;
    }
    throw Exception('error fetching posts');
  }


  @override
  Future<void> close() {
    _connetCubitSubscription.cancel();
    return super.close();
  }

}
