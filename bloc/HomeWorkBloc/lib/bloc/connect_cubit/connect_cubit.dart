// Dart imports:
import 'dart:async';

// Package imports:
import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';

enum ConnectionStatus { connected, connectLost }

class ConnetCubit extends Cubit<ConnectionStatus> {
  ConnetCubit() : super(ConnectionStatus.connected) {
    initConnectivity();
  }

  final Connectivity _connectivity = Connectivity();

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on Exception catch (e) {
      print(e.toString());
    }

    _updateConnectionStatus(result);

    //fetchData();
  }

  Future<void> checkConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on Exception catch (e) {
      print(e.toString());
    }

    _updateConnectionStatus(result);

    //fetchData();
  }

  bool checkConnectedStatus() {
    checkConnectivity();
    return (state == ConnectionStatus.connected);
  }

  bool setConnectedStatus() {
    emit(ConnectionStatus.connected);
  }

  bool setConnectedLostStatus() {
    emit(ConnectionStatus.connectLost);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setConnectedStatus();
        break;
      case ConnectivityResult.mobile:
        setConnectedStatus();
        break;
      case ConnectivityResult.none:
        setConnectedLostStatus();
        break;
      default:
        setConnectedLostStatus();
        break;
    }
  }
}
