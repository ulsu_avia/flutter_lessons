// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'navigation_event.dart';
import 'navigation_state.dart';

class NavigatorBloc extends Bloc<NavigatorEvent, MyState> {
  NavigatorBloc() : super(StateBreedsPage());

  @override
  Stream<MyState> mapEventToState(NavigatorEvent event) async* {
    switch (event) {
      case NavigatorEvent.eventBreedsPage:
        yield StateBreedsPage();
        break;
      case NavigatorEvent.eventPhotoPage:
        yield StatePhotoPage();
        break;
      case NavigatorEvent.eventFailureBreeds:
        yield StateBreedsPageFailure();
        break;
      case NavigatorEvent.eventFailurePhotos:
        yield StatePhotosPageFailure();
        break;
    }
  }
}
