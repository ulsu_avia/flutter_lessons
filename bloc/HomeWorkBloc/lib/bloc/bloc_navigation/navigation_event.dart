enum NavigatorEvent {
  eventBreedsPage,
  eventPhotoPage,
  eventFailureBreeds,
  eventFailurePhotos
}
