// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

// Project imports:
import 'navigation_event.dart';

@immutable
abstract class MyState {}

class StateBreedsPage extends MyState {}

class StatePhotoPage extends MyState {}

class StateBreedsPageFailure extends MyState {}

class StatePhotosPageFailure extends MyState {}
