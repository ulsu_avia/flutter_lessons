// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'dogs_breeds.g.dart';

@JsonSerializable(explicitToJson: true)
class Breeds {
  final Map message;
  final String status;
  //final List<String> subBreeds;

  Breeds({this.message, this.status});

  factory Breeds.fromJson(Map<String, dynamic> json) =>
      _$BreedsFromJson(json);
  Map<String, dynamic> toJson() => _$BreedsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Photos {
  final List message;
  final String status;
  //final List<String> subBreeds;

  Photos({this.message, this.status});

  factory Photos.fromJson(Map<String, dynamic> json) =>
      _$PhotosFromJson(json);
  Map<String, dynamic> toJson() => _$PhotosToJson(this);
}
//
// @JsonSerializable(explicitToJson: true)
// class DogMessage {
//   @JsonKey(defaultValue: false)
//   final Map<String, List<String>> breeds;
//
//   DogMessage({this.breeds});
//
//   factory DogMessage.fromJson(Map<String, dynamic> json) =>
//       _$DogMessageFromJson(json);
//   Map<String, dynamic> toJson() => _$DogMessageToJson(this);
// }
//
// @JsonSerializable(explicitToJson: true)
// class DogSubBreeds {
//   @JsonKey(defaultValue: false)
//   final List<String> subBreeds;
//
//   DogSubBreeds({this.subBreeds});
//
//   factory DogSubBreeds.fromJson(Map<String, dynamic> json) =>
//       _$DogSubBreedsFromJson(json);
//   Map<String, dynamic> toJson() => _$DogSubBreedsToJson(this);
// }
