// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'git_hub_jobs.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Breeds _$BreedsFromJson(Map<String, dynamic> json) {
  return Breeds(
    message: json['message'] as Map<String, dynamic>,
    status: json['status'] as String,
  );
}

Map<String, dynamic> _$BreedsToJson(Breeds instance) => <String, dynamic>{
      'message': instance.message,
      'status': instance.status,
    };

Photos _$PhotosFromJson(Map<String, dynamic> json) {
  return Photos(
    message: json['message'] as List,
    status: json['status'] as String,
  );
}

Map<String, dynamic> _$PhotosToJson(Photos instance) => <String, dynamic>{
      'message': instance.message,
      'status': instance.status,
    };
