// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:delayed_display/delayed_display.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_event.dart';
import 'package:HomeWorkBloc/bloc/bloc_photos/photos_bloc.dart';
import 'package:HomeWorkBloc/view/failure/failure.dart';
import 'package:HomeWorkBloc/widgets/bottom_loader.dart';
import 'package:HomeWorkBloc/widgets/photos_list_item.dart';

class PhotoList extends StatefulWidget {
  @override
  _PhotoListState createState() => _PhotoListState();
}

class _PhotoListState extends State<PhotoList> {
  PhotosBloc _photosBloc;

  @override
  void initState() {
    super.initState();
    _photosBloc = context.read<PhotosBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhotosBloc, PhotosState>(
      builder: (context, state) {
        switch (state.status) {
          case PhotosStatus.updated:
            return const Center(child: CircularProgressIndicator());
          case PhotosStatus.failure:
            BlocProvider.of<NavigatorBloc>(context).add(NavigatorEvent.eventFailurePhotos);
            // return const Center(child: Text('Не удалось получить данные'));
            return FailurePage();
          case PhotosStatus.success:
            if (state.photos.isEmpty) {
              return const Center(child: Text('Нет записей'));
            }
            return DelayedDisplay(
              delay: Duration(milliseconds: 800),
              child: ListView.builder(
                itemCount: state.photos.length,
                itemBuilder: (BuildContext context, int index) {
                  return index >= state.photos.length
                      ? BottomLoader()
                      : PhotosListItem(
                          photos: state.photos.elementAt(index),
                        );
                },
              ),
            );
          default:
            return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
