// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:HomeWorkBloc/view/dogs_photos/photo_list.dart';

class PhotoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Dog Breeds')),
      body: PhotoList(),
      // body:  BlocProvider.value(
      //   value: BlocProvider.of<BreedsBloc>(context),
      //   child: PhotoList(),
      // ),
    );
  }
}
