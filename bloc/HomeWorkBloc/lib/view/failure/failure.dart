// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:HomeWorkBloc/bloc/bloc_breeds/breeds_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_state.dart';
import 'package:HomeWorkBloc/bloc/bloc_photos/photos_bloc.dart';

class FailurePage extends StatefulWidget {
  @override
  _FailurePageState createState() => _FailurePageState();
}

class _FailurePageState extends State<FailurePage> {
  NavigatorBloc _navigatorBloc;
  //BlocProvider.of<NavigatorBloc>(context).add(NavigatorEvent.eventPhotoPage);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _navigatorBloc = context.read<NavigatorBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<NavigatorBloc, MyState>(
      listener: (context, state) {
        if (state is StatePhotoPage || state is StateBreedsPage) {
          //Navigator.of(context).pop();
        }
      },
      // child: Scaffold(
      //   appBar: AppBar(title: const Text('Dog Breeds')),
      //   body:
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(child: Text('Не удалось получить данные')),
            RaisedButton(
                child: Text('Повторить'),
                onPressed: () {
                  if (_navigatorBloc.state is StateBreedsPageFailure) {
                    //_navigatorBloc.add(NavigatorEvent.eventBreedsPage);
                    BlocProvider.of<BreedsBloc>(context).add(BreedsFetched());
                  } else if (_navigatorBloc.state is StatePhotosPageFailure) {
                    //_navigatorBloc.add(NavigatorEvent.eventPhotoPage);
                    BlocProvider.of<PhotosBloc>(context).add(PhotosUpdate());
                  }
                }),
          ],
        ),
      ),
    );
    //   ),
    // );
  }
}
