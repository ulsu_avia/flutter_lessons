// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:HomeWorkBloc/bloc/bloc_breeds/breeds_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_event.dart';
import 'package:HomeWorkBloc/view/failure/failure.dart';
import 'package:HomeWorkBloc/widgets/bottom_loader.dart';
import 'package:HomeWorkBloc/widgets/breeds_list_item.dart';

class BreedsList extends StatefulWidget {
  @override
  _BreedsListState createState() => _BreedsListState();
}

class _BreedsListState extends State<BreedsList> {
  final _scrollController = ScrollController();
  BreedsBloc _postBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _postBloc = context.read<BreedsBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BreedsBloc, BreedsState>(
      builder: (context, state) {
        switch (state.status) {
          case BreedsStatus.failure:
            BlocProvider.of<NavigatorBloc>(context).add(NavigatorEvent.eventFailureBreeds);
            //return Center(child: Text('Не удалось получить данные'));
            return FailurePage();
          case BreedsStatus.success:
            if (state.breeds.isEmpty) {
              return const Center(child: Text('Нет записей'));
            }
            return ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return index >= state.breeds.length
                    ? BottomLoader()
                    :
                BreedsListItem(breeds: state.breeds.keys.elementAt(index).toString(),
                  subBreeds: state.breeds.values.elementAt(index).toString(),);
              },
              itemCount: state.breeds.length,
              controller: _scrollController,
            );
          default:
            return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) _postBloc.add(BreedsFetched());
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
