// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_state.dart';
import 'package:HomeWorkBloc/view/dogs_breeds/breeds_list.dart';

class BreedsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<NavigatorBloc, MyState>(
      listener: (context, state) {
        if (state is StatePhotoPage) {
          Navigator.of(context).pushNamed('/pageB');
        }
        // if (state is StateBreedsPageFailure || state is StatePhotosPageFailure ) {
        //   //Navigator.of(context).pushReplacementNamed('/failure');
        // }
      },
      child: Scaffold(
        appBar: AppBar(title: const Text('Dog Breeds')),
         body: BreedsList(),
      ),
    );
  }
}
