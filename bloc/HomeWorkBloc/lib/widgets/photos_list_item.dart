// Flutter imports:
import 'package:flutter/material.dart';

class PhotosListItem extends StatelessWidget {
  const PhotosListItem({Key key, @required this.photos}) : super(key: key);

  final String photos;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Padding(
      padding: EdgeInsets.only(top: 5, right: 5, left: 5),
      child: Container(
        padding: EdgeInsets.all(10),
        height: 250,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fitHeight,
            image: NetworkImage(photos),
          ),
        ),
      ),
    );
  }
}
