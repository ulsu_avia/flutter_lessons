// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_event.dart';
import 'package:HomeWorkBloc/bloc/bloc_photos/photos_bloc.dart';

class BreedsListItem extends StatelessWidget {
  const BreedsListItem({Key key, @required this.breeds, @required this.subBreeds})
      : super(key: key);

  final String breeds;
  final String subBreeds;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Card(
      child: ListTile(
        //leading: Text('${dogs.id}', style: textTheme.caption),
        title: Text(breeds),
        subtitle: (subBreeds != 'null') ? Text(subBreeds) : null,
        onTap: () {
          BlocProvider.of<PhotosBloc>(context).add(PhotosFetched(breeds));
          BlocProvider.of<NavigatorBloc>(context).add(NavigatorEvent.eventPhotoPage);
        },
      ),
    );
  }
}
