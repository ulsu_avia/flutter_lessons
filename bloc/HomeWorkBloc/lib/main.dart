// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:HomeWorkBloc/bloc/bloc_breeds/breeds_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_navigation/navigation_bloc.dart';
import 'package:HomeWorkBloc/bloc/bloc_photos/photos_bloc.dart';
import 'package:HomeWorkBloc/bloc/connect_cubit/connect_cubit.dart';
import 'package:HomeWorkBloc/simple_bloc_observer.dart';
import 'package:HomeWorkBloc/view/dogs_breeds/breeds_page.dart';
import 'package:HomeWorkBloc/view/dogs_photos/photo_page.dart';
import 'package:HomeWorkBloc/view/failure/failure.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  EquatableConfig.stringify = kDebugMode;
  Bloc.observer = SimpleBlocObserver();

  Dio _client = Dio();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<ConnetCubit>(
          create: (context) => ConnetCubit()
        ),
        BlocProvider<BreedsBloc>(
          create: (context) => BreedsBloc(
            dioClient: _client, connetCubit: BlocProvider.of<ConnetCubit>(context),
          )..add(BreedsFetched()),
        ),
        BlocProvider<PhotosBloc>(
          create: (context) => PhotosBloc(
            dioClient: _client, connetCubit: BlocProvider.of<ConnetCubit>(context),
          ),
        ),
        BlocProvider<NavigatorBloc>(
          create: (context) => NavigatorBloc(),
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        '/': (context) => BreedsPage(),
        '/pageB': (context) => PhotoPage(),
        '/failure': (context) => FailurePage(),
      },
      initialRoute: '/',
    );
  }
}
