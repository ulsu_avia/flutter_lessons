package com.example.location_plugin

import androidx.annotation.NonNull
import LocationController
import android.util.Log

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import kotlin.math.absoluteValue

/** LocationPlugin */
class LocationPlugin: FlutterPlugin, MethodCallHandler, EventChannel.StreamHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var locationController : LocationController
  private lateinit var eventChannel: EventChannel
  private lateinit var msg: String

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "location_plugin")
    locationController = LocationController(flutterPluginBinding.applicationContext)

    channel.setMethodCallHandler(this)
    msg = "Start"
    eventChannel = EventChannel(flutterPluginBinding.binaryMessenger, "location_plugin_events")
    eventChannel.setStreamHandler(this)

    locationController.checkLocation()
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    } else if (call.method== "getLocation"){
      getLocation(result)
    } else if (call.method== "getLocationUpdate"){
      getLocationUpdates(result)
    }else if (call.method== "getMessage"){
      getMessage(result)
    }else if (call.method== "startLoc"){
      startLoc(result)
    }else if (call.method== "stopLoc"){
      stopLoc(result)
    }else if (call.method== "getActualLatitude"){
      getActualLatitude(result)
    } else if (call.method== "getActualLongitude"){
      getActualLongitude(result)
    } else if (call.method== "getActualTime"){
      getActualTime(result)
    } else if (call.method== "setSmallestDisplacement"){

      var locSmallestDisplacement : String
      val smallestDisplacement : String? = call.argument("smallestDisplacement")
      if (smallestDisplacement != null) {
        locSmallestDisplacement = smallestDisplacement as String
        var locFloatSmallestDisplacement = locSmallestDisplacement.toFloat()
        setSmallestDisplacement(result, locFloatSmallestDisplacement)
      }

    }  else {
      result.notImplemented()
    }
  }

  private fun getLocation(@NonNull result: Result) {
    try {
      result.success(locationController.getActualLocation())
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  private fun getActualLatitude(@NonNull result: Result) {
    try {
      result.success(locationController.getActualLatitude())
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  private fun getActualLongitude(@NonNull result: Result) {
    try {
      result.success(locationController.getActualLongitude())
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  private fun getActualTime(@NonNull result: Result) {
    try {
      result.success(locationController.getActualTime())
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  private fun getLocationUpdates(@NonNull result: Result) {
    try {
      result.success(locationController.getLocationUpdates())
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  private fun getMessage(@NonNull result: Result) {
    try {
      //result.success(locationController.checkLocation())
      result.success(msg)
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  private fun startLoc(@NonNull result: Result) {
    try {
      result.success(locationController.startLocationUpdates())
      //result.success(msg)
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  private fun stopLoc(@NonNull result: Result) {
    try {
      result.success(locationController.stopLocationUpdates())
      //result.success(msg)
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  private fun setSmallestDisplacement(@NonNull result: Result, smallestDisplacement : Float) {
    try {
      result.success(locationController.setSmallestDisplacement(smallestDisplacement))
      //result.success(msg)
    }
    catch (e: Exception)
    {
      result.error("NO_MAP_ACCESS", null, null)
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    locationController.dispose()
    channel.setMethodCallHandler(null)
    eventChannel.setStreamHandler(null)
  }

  override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
    locationController.setOnChangeListener(
            object : LocationController.StateChangeListener
            {
              override fun onChange(strLocation: String) {
                events?.success(strLocation)
              }
            }
    )
  }


  override fun onCancel(arguments: Any?)
  {
    locationController.setOnChangeListener(null)
  }

}
