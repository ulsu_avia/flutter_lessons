//import android.R.layout

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.HandlerThread
import android.os.Looper

import com.google.android.gms.location.*
import java.text.SimpleDateFormat
import java.util.*


class LocationController(appContext: Context) {
  private var context = appContext
  private lateinit var fusedLocationClient: FusedLocationProviderClient
  private lateinit var locationRequest: LocationRequest
  private lateinit var locationCallback: LocationCallback
  private var strLocation : String = ""
  private var listener: StateChangeListener? = null

  private var address: String = ""
  private var addresNum: String = ""
  private var city: String = ""
  private var state: String = ""
  private var country: String = ""
  private var postalCode: String = ""
  private var knownName: String = ""
  private var latitude: String = ""
  private var longitude: String = ""
  private var dateUpdate: String = ""
  private var smallestDisplacement: Float = 170f
  private var handlerThread: HandlerThread = HandlerThread("MyHandlerThread")
  private lateinit var looper : Looper

  fun checkLocation(): String{
    handlerThread = HandlerThread("MyHandlerThread")
    handlerThread.start()
    looper = handlerThread.getLooper()
    fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    getLocationUpdates()

    return ""
  }

  fun getLocationUpdates() : String {
    fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    locationRequest = LocationRequest()
    locationRequest.interval = 5000
    locationRequest.fastestInterval = 5000
    locationRequest.smallestDisplacement = smallestDisplacement //170 m = 0.1 mile
    locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY //according to your app
    locationCallback = object : LocationCallback() {
      override fun onLocationResult(locationResult: LocationResult?) {
        locationResult ?: return
        if (locationResult.locations.isNotEmpty()) {
          /*val location = locationResult.lastLocation
          Log.e("location", location.toString())*/
          val addresses: List<Address>?
          val geoCoder = Geocoder(context, Locale.getDefault())
          addresses = geoCoder.getFromLocation(
                  locationResult.lastLocation.latitude,
                  locationResult.lastLocation.longitude,
                  1
          )

          latitude = locationResult.lastLocation.latitude.toString()
          longitude = locationResult.lastLocation.longitude.toString()

          if (addresses != null && addresses.isNotEmpty()) {
             //address = addresses[0].getAddressLine(0)
             if(addresses[0]?.getThoroughfare() != null)  address = addresses[0]?.getThoroughfare() + "," else address = ""
             if(addresses[0]?.getSubThoroughfare() != null)  addresNum = addresses[0]?.getSubThoroughfare() + "," else addresNum = ""
             city = addresses[0]?.locality
             state = addresses[0]?.adminArea
             country = addresses[0]?.countryName
             postalCode = addresses[0]?.postalCode
             knownName = addresses[0]?.featureName
             strLocation = "$address $addresNum $city, $state, $country, $postalCode"
            val date = getCurrentDateTime()
            dateUpdate = date.toString("HH:mm:ss")
          }
        }
      }


    }

    return strLocation
  }

  fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
    val formatter = SimpleDateFormat(format, locale)
    return formatter.format(this)
  }

  fun getCurrentDateTime(): Date {
    return Calendar.getInstance().time
  }

  // Start location updates
  fun startLocationUpdates() : Boolean{
    fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            looper
    )


    return true
  }

  // Stop location updates
  fun stopLocationUpdates() : Boolean{
    fusedLocationClient.removeLocationUpdates(locationCallback)

    return false;
  }

  fun setSmallestDisplacement(locSmallestDisplacement: Float) : String{

    smallestDisplacement = locSmallestDisplacement.toFloat();
    return smallestDisplacement.toString()

  }

  interface StateChangeListener {
    fun onChange(strLocation: String)
  }

  fun dispose() {
    stopLocationUpdates()
    //cameraManager.unregisterTorchCallback(torchCallback)
  }

  fun setOnChangeListener(listener: StateChangeListener?) {
    this.listener = listener
    listener?.onChange(strLocation)
  }

  fun getActualLocation(): String {
    return strLocation
  }


  fun getActualLatitude(): String {
    return latitude
  }

  fun getActualLongitude(): String {
    return longitude
  }

  fun getActualTime(): String {
    return dateUpdate
  }


}