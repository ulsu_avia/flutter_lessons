
import 'dart:async';
import 'dart:ffi';

import 'package:flutter/services.dart';

class LocationPlugin {
  static const MethodChannel _channel =
      const MethodChannel('location_plugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> getMessage() => _channel.invokeMethod<String>('getMessage');

  static Future<String> getLocation() => _channel.invokeMethod<String>('getLocation');

  static Future<bool> startLoc() => _channel.invokeMethod<bool>('startLoc');

  static Future<bool> stopLoc() => _channel.invokeMethod<bool>('stopLoc');

  static Future<String> getActualLatitude() => _channel.invokeMethod<String>('getActualLatitude');

  static Future<String> getActualLongitude() => _channel.invokeMethod<String>('getActualLongitude');

  static Future<String> getActualTime() => _channel.invokeMethod<String>('getActualTime');

  static Future<String> setSmallestDisplacement(String smallestDisplacement) => _channel.invokeMethod<String>('setSmallestDisplacement', {'smallestDisplacement': smallestDisplacement});

  static Future<String> getLocationUpdate() => _channel.invokeMethod<String>('getLocationUpdate');

  static const EventChannel _eventChannel = const EventChannel('location_plugin_events');

  static Stream<String> _locationStateStream = _eventChannel.receiveBroadcastStream().cast<String>();

  static Stream<String> get locationStateStream => _locationStateStream;
}
