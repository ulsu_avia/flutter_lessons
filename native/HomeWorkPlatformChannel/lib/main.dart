import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:location_plugin/location_plugin.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:async';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _controllerDisplacement = new TextEditingController();

  Map<Permission, PermissionStatus> statuses;

  var _strLocation;
  var _strLatitude;
  var _strLongitude;
  var _strLatitudeLongitude;
  var _strTime;

  String errorMessage = '';

  bool _enabledLoc = false;

  Future<void> checkPermission() async {
    statuses = await [
      Permission.location,
      Permission.locationAlways,
      Permission.locationWhenInUse,
    ].request();
  }

  @override
  void initState() {
    if (_controllerDisplacement.text != null) {
      _controllerDisplacement.text = "0.0";
    }
    // TODO: implement initState
    checkPermission();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Текущее месторасположение"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: StreamBuilder<String>(
          stream: LocationPlugin.locationStateStream,
          initialData: "",
          builder: (context, snapshot) {
            if (_enabledLoc) getLocation();
            return Center(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      _strLocation ?? "Нет данных",
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      _strLatitudeLongitude != null
                          ? "\n$_strLatitudeLongitude\n"
                          : "",
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      _strTime != null ? "Обновлено: $_strTime" : "",
                      textAlign: TextAlign.center,
                    ),
                    if (!_enabledLoc)
                      Form(
                        key: _formKey,
                        child: TextFormField(
                          key: Key('fieldDist'),
                          controller: _controllerDisplacement,
                          decoration: InputDecoration(
                              labelText: 'Наименьшее смещение в метрах',
                              labelStyle: TextStyle(
                                fontSize: 20.0,
                                height: 1.0,
                              )),
                          validator: (value) {
                            if (value == null) return 'Введите значение';
                            double _parsedValue = double.tryParse(value);
                            if (_parsedValue == null)
                              return 'Значение не корректно';
                            return null;
                          },
                          keyboardType:
                              TextInputType.numberWithOptions(decimal: true),
                        ),
                      ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          startStopLoc();
        },
        child: _enabledLoc ? Icon(Icons.pause) : Icon(Icons.play_arrow),
      ),
    );
  }

  Future<void> getLocation() async {
    LocationPlugin.setSmallestDisplacement(
        _controllerDisplacement.text.toString());

    //await new Future.delayed(const Duration(seconds: 5));
    final location = await LocationPlugin.getLocation();
    setState(() => _strLocation = location);

    final latitude = await LocationPlugin.getActualLatitude();
    setState(() => _strLatitude = latitude);

    final longitude = await LocationPlugin.getActualLongitude();
    setState(() => _strLongitude = longitude);

    _strLatitudeLongitude = "$_strLatitude, $_strLongitude";

    final time = await LocationPlugin.getActualTime();
    setState(() => _strTime = time);
  }

  Future<void> startStopLoc() async {
    if (_enabledLoc) {
      final enabledLoc = await LocationPlugin.stopLoc();
      setState(() => _enabledLoc = enabledLoc);
    } else {
      if (_formKey.currentState.validate()) {
        final enabledLoc = await LocationPlugin.startLoc();
        setState(() => _enabledLoc = enabledLoc);
      }
    }
  }

  Future<void> getMessage() async {
    final msg = await LocationPlugin.getMessage();
    setState(() => _strLocation = msg);
  }
}
