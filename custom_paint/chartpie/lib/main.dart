import 'package:custompaint/piechart.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      //showPerformanceOverlay: true,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _piePeaces = new TextEditingController();
  List<String> result = [];

  bool checkText(String text) {
    try {
      bool validateText = true;

      List<String> newPieChart = text.split(",");

      for (var i = 0; i < newPieChart.length; i++) {
        var size = double.tryParse(newPieChart[i]);
        if (size == null) validateText = false;
      }
      return validateText;
    } catch (e) {
      return false;
    }
  }

  List<PieChartItem> BuildPiePieces(List<String> numbers) {
    List<PieChartItem> newPieChart = [];
    double sumOfItems = 0;

    for (var i = 0; i < numbers.length; i++) {
      var size = double.tryParse(numbers[i]);
      sumOfItems = size + sumOfItems;
    }

    for (var i = 0; i < numbers.length; i++) {
      var size = double.tryParse(numbers[i]);
      newPieChart.add(
        PieChartItem(
          ((size / sumOfItems) * 100).ceil(),
          i.toString(),
          Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
              .withOpacity(1.0),
          numbers[i],
        ),
      );
    }
    return newPieChart;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _piePeaces.text = '10, 5, 6, 18, 20';
    result = _piePeaces.text.split(",");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Center(
                // Center is a layout widget. It
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ViewingPieChart(
                    items: BuildPiePieces(result),
                    toText: (item, _) => TextPainter(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          style: TextStyle(color: Colors.black, fontSize: 8.0),
                          text: "${item.number}",
                        ),
                        textDirection: TextDirection.ltr),
                    opacity: 0.0,
                  ),
                ),
              ),
              Center(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Form(
                        key: _formKey,
                        child: TextFormField(
                          key: Key('fieldDist'),
                          controller: _piePeaces,
                          decoration: InputDecoration(
                              labelText: 'Введите числа',
                              labelStyle: TextStyle(
                                fontSize: 20.0,
                                height: 1.0,
                              )),
                          validator: (value) {
                            if (value == null) return 'Введите значение';
                            if (!checkText(value))
                              return 'Значение не корректно';
                            return null;
                          },
                          keyboardType:
                              TextInputType.numberWithOptions(decimal: true),
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(elevation: 2),
                        onPressed: () {
                          try {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                result = _piePeaces.text.split(",");
                              });
                            }
                          } catch (e) {}
                        },
                        child: Text('Обновить pie chart'),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
