import 'dart:math';

import 'package:flutter/material.dart';

class PieChartItem {
  final num val;
  final String name;
  final Color color;
  final String number;

  PieChartItem(this.val, this.name, this.color, this.number) : assert(val != 0);
}

typedef TextPainter PieChartItemToText(PieChartItem item, double total);

class ViewingPieChart extends StatelessWidget {
  final double accellerationFactor;
  final List<PieChartItem> items;
  final PieChartItemToText toText;
  final double opacity;

  const ViewingPieChart(
      {Key key,
      this.accellerationFactor = 1.0,
      @required this.items,
      @required this.toText,
      this.opacity})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AspectRatio(
        aspectRatio: 1.0,
        child: _RotatingPieChartInternal(
          items: items,
          toText: toText,
          accellerationFactor: accellerationFactor,
          opacity: opacity,
        ),
      ),
    );
  }
}

class _RotationEndSimulation extends Simulation {
  final double initialVelocity;
  final double initialPosition;
  final double accelleration;

  _RotationEndSimulation({
    @required this.initialVelocity,
    @required double decelleration,
    @required this.initialPosition,
  }) : accelleration = decelleration * -1.0;

  @override
  double dx(double time) => initialVelocity + (accelleration * time);

  @override
  bool isDone(double time) =>
      initialVelocity > 0 ? dx(time) < 0.001 : dx(time) > -0.001;

  @override
  double x(double time) =>
      (initialPosition +
          (initialVelocity * time) +
          (accelleration * time * time / 2)) %
      1.0;
}

class _RotatingPieChartInternal extends StatefulWidget {
  final double accellerationFactor;
  final List<PieChartItem> items;
  final PieChartItemToText toText;
  final double opacity;

  const _RotatingPieChartInternal(
      {Key key,
      this.accellerationFactor = 1.0,
      @required this.items,
      @required this.toText,
      this.opacity})
      : super(key: key);

  @override
  _RotatingPieChartInternalState createState() =>
      _RotatingPieChartInternalState();
}

class _RotatingPieChartInternalState extends State<_RotatingPieChartInternal>
    with SingleTickerProviderStateMixin {

  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    _controller = AnimationController(vsync: this);
    _animation = new Tween(begin: 0.0, end: 1.0).animate(_controller);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Offset lastDirection;

  Offset getDirection(Offset globalPosition) {
    RenderBox box = context.findRenderObject();
    Offset offset = box.globalToLocal(globalPosition);
    Offset center = Offset(context.size.width / 2.0, context.size.height / 2.0);
    return offset - center;
  }

  void primitiveUpdateOpacity()
  {
    setState(() {
      _controller.value = 0.0;
    });
    _controller.animateTo(1.0, duration: Duration(seconds: 3));
  }

  @override
  Widget build(BuildContext context) {
    primitiveUpdateOpacity();

    return AnimatedBuilder(
      animation: _animation,
      builder: (context, widget) {
        return Stack(
          fit: StackFit.passthrough,
          children: [
            Opacity(
              opacity: _controller.value,
              child: widget,
            ),
            Opacity(
              opacity: _controller.value,
              child:  CustomPaint(
                painter: _PieTextPainter(
                    items: this.widget.items,
                    toText: this.widget.toText),
              ),
            ),
          ],
        );
      },
      child: CustomPaint(
        painter: _PieChartPainter(
          items: widget.items,
        ),
      ),
    );
  }
}

abstract class _AlignedCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    FittedSizes fittedSizes =
        applyBoxFit(BoxFit.contain, Size(100.0, 100.0), size);
    var dest = fittedSizes.destination;
    canvas.translate(
        (size.width - dest.width) / 2 + 1, (size.height - dest.height) / 2 + 1);
    canvas.scale((dest.width - 2) / 100.0);
    alignedPaint(canvas, Size(100.0, 100.0));
  }

  void alignedPaint(Canvas canvas, Size size);
}

class _PieChartPainter extends _AlignedCustomPainter {
  final List<PieChartItem> items;
  final double total;
  final double rotation;

  _PieChartPainter({this.rotation = 0.0, @required this.items})
      : total = items.fold(0.0, (total, el) => total + el.val);

  @override
  void alignedPaint(Canvas canvas, Size size) {
    Rect rect = Offset.zero & size;

    double soFar = rotation;
    Paint outlinePaint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.stroke;

    for (int i = 0; i < items.length; ++i) {
      PieChartItem item = items[i];
      double arcRad = item.val / total * 2 * pi;
      canvas.drawArc(rect, soFar, arcRad, true, Paint()..color = item.color);
      canvas.drawArc(rect, soFar, arcRad, true, outlinePaint);
      soFar += arcRad;
    }
  }

  @override
  bool shouldRepaint(_PieChartPainter oldDelegate) {
    return oldDelegate.rotation != rotation || oldDelegate.items != items;
  }
}

class _PieTextPainter extends _AlignedCustomPainter {
  final List<PieChartItem> items;
  final double total;
  final List<double> middles;
  final PieChartItemToText toText;
  static final double textDisplayCenter = 0.7;

  _PieTextPainter._(
      this.items, this.total, this.middles, this.toText);

  factory _PieTextPainter(
      {@required List<PieChartItem> items,
      @required PieChartItemToText toText}) {
    double total = items.fold(0.0, (prev, el) => prev + el.val);
    var middles = (() {
      double soFar = 0.0;
      return items.map((item) {
        double arcRad = item.val / total * 2 * pi;
        double middleRad = (soFar) + (arcRad / 2);
        soFar += arcRad;
        return middleRad;
      }).toList(growable: false);
    })();
    return _PieTextPainter._(items, total, middles, toText);
  }

  @override
  void alignedPaint(Canvas canvas, Size size) {
    for (int i = 0; i < items.length; ++i) {
      var middleRad = middles[i];
      var item = items[i];
      var rad = size.width / 2;

      var middleX = rad + rad * textDisplayCenter * cos(middleRad);
      var middleY = rad + rad * textDisplayCenter * sin(middleRad);

      TextPainter textPainter = toText(item, total)..layout();
      textPainter.paint(
          canvas,
          Offset(middleX - (textPainter.width / 2),
              middleY - (textPainter.height / 2)));
    }
  }

  @override
  bool shouldRepaint(_PieTextPainter oldDelegate) {
    return oldDelegate.items != items ||
        oldDelegate.toText != toText;
  }
}
