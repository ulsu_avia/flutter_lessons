import 'dart:async';
import 'dart:convert';
import 'package:albums_route/list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:albums_route/fetch_file.dart';
import 'package:albums_route/albums.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      onUnknownRoute: (RouteSettings settings) {
        return MaterialPageRoute(builder: (BuildContext context) {
          return NotFound();
        });
      },
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case HomePage.routeName:
            return MaterialPageRoute(builder: (BuildContext context) {
              return HomePage();
            });
          case Albums.routeName:
            final args = settings.arguments as Map<String, dynamic>;

            if (args != null && args.containsKey('id')) {
              return MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (BuildContext context) {
                    return Albums(id: args['id']);
                  });
            }

            return MaterialPageRoute(builder: (BuildContext context) {
              return Albums();
            });
          case AlbumsData.routeName:
            final args = settings.arguments as Map<String, dynamic>;

            if (args != null && args.containsKey('description') && args.containsKey('album')) {
              return MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (BuildContext context) {
                    return AlbumsData(description: args['description'],album: args['album']);
                  });
            }
            return MaterialPageRoute(builder: (BuildContext context) {
              return AlbumsData();
            });
          default:
            return MaterialPageRoute(builder: (BuildContext context) {
              return NotFound();
            });
        }
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      //home: MyHomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  static const routeName = '/';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(child: Column(
        children: <Widget>[
          DrawerHeader(
            child: Column(
              children: <Widget>[
                CircleAvatar(
                  radius: 65,
                  backgroundColor: Colors.white,
                  backgroundImage:
                  NetworkImage('https://cdn.icon-icons.com/icons2/510/PNG/512/ios7-musical-notes_icon-icons.com_50221.png'),
                ),
              ],
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            trailing: Icon(Icons.arrow_forward),
            title: Text("Home"),
            onTap: () {
              Navigator.of(context).pushNamed('/');
            },
            onLongPress: () {
              print("ON LONG PRESS");
            },
          ),
          ListTile(
            leading: Icon(Icons.album),
            trailing: Icon(Icons.arrow_forward),
            title: Text("Albums"),
            onTap: () {
              Navigator.of(context).pushNamed('/album');
            },
            onLongPress: () {
              print("ON LONG PRESS");
            },
          )
        ],
      ),
      ),
      appBar: AppBar(
        title: Text('Lesson 7'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Описание ДЗ: \nСамостоятельно постройте навигацию и напишите анимацию перехода между экранами.",
              style: TextStyle(color: Colors.black,fontSize: 20),
              textAlign: TextAlign.center,
              ),
          ],
        ),
      ),
    );
  }
}


class Albums extends StatefulWidget {
  static const routeName = '/album';

  Albums({Key key, this.id}) : super(key: key);

  final int id;

  @override
  _AlbumsState createState() => _AlbumsState();
}

class _AlbumsState extends State<Albums> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Albums'),
        //automaticallyImplyLeading: false,
      ),
      body: Container(
        child: Center(
          // Use future builder and DefaultAssetBundle to load the local JSON file
          child: FutureBuilder<List<Album>>(
            future: fetchFileFromAssets('assets/artists.json'),
            builder: (context, snapshot) {
              if (snapshot.hasError) print(snapshot.error);

              return snapshot.hasData
                  ? AlbumList(album: snapshot.data)
                  : Center(child: CircularProgressIndicator());
            },
          ),
        ),
      )
    );
  }
}

class AlbumsData extends StatefulWidget {
  static const routeName = '/description';

  String description;
  String album;

  AlbumsData({Key key, this.description, this.album}) : super(key: key);

  @override
  _AlbumsDataState createState() => _AlbumsDataState();
}

class _AlbumsDataState extends State<AlbumsData> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.album),
          //automaticallyImplyLeading: false,
        ),
        body: SingleChildScrollView(child: Container(
            // Use future builder and DefaultAssetBundle to load the local JSON file
            child: Text(
              widget.description,
              style: TextStyle(color: Colors.black,fontSize: 15),
              textAlign: TextAlign.justify,
              textDirection: TextDirection.ltr,
            ),

          ),)
        );
  }
}

class NotFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Page not found'),
      ),
    );
  }
}

