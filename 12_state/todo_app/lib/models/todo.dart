import 'package:flutter/cupertino.dart';

class ToDo{

  UniqueKey id;
  String title;
  bool isDone;

  ToDo({this.isDone, this.title, this.id});

}