bool filterToDo(String todoTitle, String search) {
  RegExp exp = new RegExp(
    search,
    caseSensitive: false,
  );
  return exp.hasMatch(todoTitle);
}