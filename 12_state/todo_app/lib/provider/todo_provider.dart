import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/utils/filter_todo.dart';
import '../models/todo.dart';

class ToDoProvider extends ChangeNotifier {

  bool _isDoneView = false;

  Key _addTabBarKey;
  Key _doneTabBarKey;

  bool get isDoneView => _isDoneView;

  Key get addTabBarKey => _addTabBarKey;
  Key get doneTabBarKey => _doneTabBarKey;

  List<ToDo> _todo = [];

  List<ToDo> get todo => _todo;

  TextEditingController _addToDo = TextEditingController();
  TextEditingController _searchToDo = TextEditingController();

  TextEditingController get addToDo => _addToDo;
  TextEditingController get searchToDo => _searchToDo;

  void add(ToDo newTodo) {
    if(newTodo.title != '') {
      _todo.add(newTodo);
      notifyListeners();
    }
  }

  void remove(UniqueKey uniqueKey) {
      _todo.removeWhere((element) => element.id == uniqueKey);
      notifyListeners();
  }

  void setSearchText() {
      notifyListeners();
  }

  void setDoneView(bool setDoneView) {
    _isDoneView = setDoneView;
    notifyListeners();
  }

  done(UniqueKey id) {
    ToDo findTodo = _todo.firstWhere((element) => element.id == id);
    findTodo.isDone = !findTodo.isDone;
    notifyListeners();
  }

  String appBarText()
  {
    return (_isDoneView) ? 'Найти' : 'Добавить задачу';
  }

  Color primaryColor()
  {
    return (_isDoneView) ? Colors.indigo : Colors.black;
  }

  TextEditingController primaryTextController()
  {
    return (_isDoneView) ? searchToDo : addToDo;
  }

  void clearTextController()
  {
     (_isDoneView) ? searchToDo.text = '' : addToDo.text = '';
     notifyListeners();
  }

  double underlineWidth()
  {
    return (_isDoneView) ? 4 : 2;
  }
}
