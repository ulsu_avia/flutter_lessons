import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo.dart';
import 'package:todo_app/provider/todo_provider.dart';

class AppBarTaskViewWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ToDoProvider>(
      builder: (context, vm, child) {
        return Container(
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                    child: Container(
                      //padding: EdgeInsets.only(bottom: 15),
                      //height: 25,
                      //width: double.infinity,
                      //width: double.maxFinite,
                      // padding: const EdgeInsets.only(
                      //     top: 5, left: 5, bottom: 5),
                      child: Theme(
                        data: ThemeData(
                          primaryColor: vm.primaryColor(),
                          primaryColorDark: vm.primaryColor(),
                        ),
                        child: TextField(
                          keyboardType: TextInputType.multiline,
                          maxLines: 1,
                          controller: vm.primaryTextController(),
                          style: TextStyle(fontSize: 20.0, color: Colors.white),
                          //controller: myController,
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Colors.blue, width: vm.underlineWidth())),
                            hintText: vm.appBarText(),
                            labelText: vm.appBarText(),
                            hintStyle:
                                TextStyle(fontSize: 20.0, color: Colors.grey),
                            labelStyle:
                                TextStyle(fontSize: 20.0, color: Colors.grey),
                            //hasFloatingPlaceholder: false,
                            isDense: true,
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            contentPadding: EdgeInsets.symmetric(vertical: 0),
                            suffixIcon: (vm.isDoneView)
                                ? Icon(
                                    Icons.search,
                                    color: Colors.blue,
                                  )
                                : null,
                          ),
                          onChanged: (text) {
                            if (vm.isDoneView) {
                              vm.setSearchText();
                            }
                            ;
                          },
                        ),
                    ),
                  ),
                ),
                if (!vm.isDoneView)
                  Container(
                    height: 40,
                    width: 100,
                    // padding: const EdgeInsets.only(
                    //     top: 5, right: 5, bottom: 5),
                    child: RaisedButton(
                      onPressed: () {
                        vm.add(ToDo(
                            isDone: false,
                            id: UniqueKey(),
                            title: vm.addToDo.text));
                        vm.clearTextController();
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(2.0),
                        ),
                      ),
                      color: Colors.white,
                      child: Text(
                        "Добавить",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        );
      },
    );
  }
}
