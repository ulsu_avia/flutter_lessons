import 'package:todo_app/provider/todo_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TodoView extends StatelessWidget {
  const TodoView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<ToDoProvider>(
        builder: (context, vm, child) {
          return ListView(
            key: PageStorageKey(vm.addTabBarKey),
            children: vm.todo
                .map(
                  (todo) => CheckboxListTile(
                value: todo.isDone,
                onChanged: (v) {
                  vm.done(todo.id);
                },
                title: Text(todo.title),
              ),
            )
                .toList(),
          );
        },
      ),
    );
  }
}
