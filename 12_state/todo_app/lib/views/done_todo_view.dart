import 'package:todo_app/provider/todo_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/widgets/alerts.dart';
import 'package:todo_app/utils/filter_todo.dart';

class DoneTodoView extends StatelessWidget {
  const DoneTodoView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<ToDoProvider>(
        builder: (context, vm, child) {
          return ListView(
            key: PageStorageKey(vm.doneTabBarKey),
            children: vm.todo
                .where((element) => element.isDone)
                  .where((element) => filterToDo(element.title, vm.searchToDo.text))
                .map(
                  (todo) => ListTile(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alertDialog(context);
                        },
                      ).then((value) => {
                            if (value)
                              {
                                vm.remove(todo.id),
                              }
                          });
                    },
                    title: Text(todo.title),
                  ),
                )
                .toList(),
          );
        },
      ),
    );
  }
}
