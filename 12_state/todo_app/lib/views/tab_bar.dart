import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo.dart';
import 'package:todo_app/views/todo_view.dart';
import 'package:todo_app/views/done_todo_view.dart';
import 'package:todo_app/provider/todo_provider.dart';
import 'app_bar_task.dart';

class TabBarViewWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Consumer<ToDoProvider>(
        builder: (context, vm, child) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: vm.primaryColor(),
              title: AppBarTaskViewWidget(),
              bottom: TabBar(
                indicatorColor: Colors.blue,
                indicatorWeight: 5.0,
                labelColor: Colors.white,
                labelPadding: EdgeInsets.only(top: 10.0),
                unselectedLabelColor: Colors.grey,
                labelStyle: TextStyle(fontSize: 18.0),
                tabs: [
                  Tab(
                    text: 'Список задач',
                    iconMargin: EdgeInsets.only(bottom: 10.0),
                  ),
                  //child: Image.asset('images/android.png'),
                  Tab(
                    text: 'Выполненные',
                    iconMargin: EdgeInsets.only(bottom: 10.0),
                  ),
                ],
                onTap: (index) {
                  vm.setDoneView((index == 0) ? false : true);
                },
              ),
            ),
            body: TabBarView(
              children: [
                Center(
                    child: TodoView(),
                ),
                Center(
                  child: DoneTodoView(),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
