import 'package:flutter/material.dart';

AlertDialog alertDialog(BuildContext context) {
  // set up the button
  Widget yesButton = ListTile(
    onTap: () {
      Navigator.of(context).pop(true);
    },
    title: Text('Да'),
  );

  // set up the button
  Widget noButton = ListTile(
    onTap: () {
      Navigator.of(context).pop(false);
    },
    title: Text('Нет'),
  );

  // set up the AlertDialog
  return AlertDialog(
    title: Text("Вы уверены, что хотите удалить задачу?"),
    content: Container(
      height: 100,
      //padding: EdgeInsets.all(5),
      width: double.maxFinite,
      child: ListView(
        children: <Widget>[
          yesButton,
          noButton,
        ],
      ),
    ),
  );
}