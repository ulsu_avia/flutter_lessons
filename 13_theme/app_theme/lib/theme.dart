import 'package:flutter/material.dart';

ThemeData _themeLight = ThemeData.light();

ThemeData themeLight = _themeLight.copyWith(
  accentColor: Colors.grey,
  primaryColor: Colors.deepOrange[100],
  primaryColorDark: Colors.deepOrange[100],
  appBarTheme: _themeLight.appBarTheme.copyWith(
    color: Colors.black54,
    textTheme: _appBarTextLight(_themeLight.textTheme),
    iconTheme: IconThemeData(
      color: Colors.deepOrange[100],
    ),
  ),

  bottomNavigationBarTheme: _themeLight.bottomNavigationBarTheme.copyWith(
    selectedItemColor: Colors.deepOrange[100],
    unselectedItemColor: Colors.black,
    backgroundColor: Colors.black54,
  ),

  cardColor: Colors.deepOrange[100],
  cardTheme: _themeLight.cardTheme.copyWith(
    clipBehavior: Clip.antiAliasWithSaveLayer,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(20),
      ),
    ),
  ),

  buttonTheme: _themeLight.buttonTheme.copyWith(
    buttonColor: Colors.deepOrange[100],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(20),
      ),
    ),
  ),

  inputDecorationTheme: _themeLight.inputDecorationTheme.copyWith(
    enabledBorder: UnderlineInputBorder(
        borderSide: new BorderSide(color: Colors.grey, width: 2)),
    focusedBorder: UnderlineInputBorder(
        borderSide: new BorderSide(color: Colors.deepOrange[100], width: 2)),
    focusColor: Colors.deepOrange[100],
    hintStyle: TextStyle(
      color: Colors.deepOrange[100],
    ),
  ),

  chipTheme: ChipThemeData(
    showCheckmark: false,
    backgroundColor: Colors.black54,
    disabledColor: Colors.grey,
    selectedColor: Colors.deepOrange[100],
    selectedShadowColor: Colors.grey,
    shadowColor: Colors.grey,
    secondarySelectedColor: Colors.grey,
     padding:  const EdgeInsets.all(1.0),
    shape: RoundedRectangleBorder(
       borderRadius: BorderRadius.all(
         Radius.circular(5),
       ),
     ),
    labelStyle: TextStyle(color: Colors.white, fontSize: 15),
     secondaryLabelStyle: TextStyle(color: Colors.white, fontSize: 15),
     brightness: Brightness.light,
    ),
    dialogBackgroundColor: Colors.grey,
    indicatorColor: Colors.grey,

  textTheme: _textLight(
    _themeLight.textTheme,
  ),
);

TextTheme _textLight(TextTheme base) {
  return base.copyWith(
    headline4: base.headline4.copyWith(
      color: Colors.grey,
      fontSize: 25,
      fontWeight: FontWeight.bold,
    ),
  );
}

TextTheme _appBarTextLight(TextTheme base) {
  return base.copyWith(
      headline6: _themeLight.textTheme.headline6.copyWith(
        color: Colors.deepOrange[100],
        fontSize: 20,
    ),
  );
}
