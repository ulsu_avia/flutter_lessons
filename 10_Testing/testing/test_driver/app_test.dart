// Imports the Flutter Driver API.
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Login form tests', () {
    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    final fieldFindEmail = find.byValueKey('fieldEmail');
    final fieldFindPhone = find.byValueKey('fieldPhone');
    final fieldFindSubmit = find.text('Отправить');

    test('Test email field',() async {
      await driver.tap(fieldFindEmail);
      await driver.waitFor(find.text(''));
      await driver.enterText('test@test.com');
      await driver.waitFor(find.text('test@test.com'));
    });

    test('Test phone field',() async {
      await driver.tap(fieldFindPhone);
      await driver.waitFor(find.text(''));
      await driver.enterText('8888888888');
      await driver.waitFor(find.text('8888888888'));
    });

    test('Test submit field',() async {
      await driver.tap(fieldFindSubmit);
    });

    test('Success text', () async {
      final success = find.text('Добро пожаловать');
      expect(await driver.getText(success), 'Добро пожаловать');
    });
  });

  group('Rigister form tests', () {
    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    final fieldFirstName = find.byValueKey('fieldFirstName');
    final fieldLastName = find.byValueKey('fieldLastName');
    final fieldFindEmail = find.byValueKey('fieldEmail');
    final fieldFindPhone = find.byValueKey('fieldPhone');
    final fieldFindSubmit = find.text('Отправить');
    final fieldEnter = find.byValueKey('enter');

    test('Test register field',() async {
      await driver.tap(fieldEnter);
    });

    test('Test first name field',() async {
      await driver.tap(fieldFirstName);
      await driver.waitFor(find.text(''));
      await driver.enterText('Alexey');
      await driver.waitFor(find.text('Alexey'));
    });

    test('Test last name field',() async {
      await driver.tap(fieldLastName);
      await driver.waitFor(find.text(''));
      await driver.enterText('Blyumenshteyn');
      await driver.waitFor(find.text('Blyumenshteyn'));
    });

    test('Test email field',() async {
      await driver.tap(fieldFindEmail);
      await driver.waitFor(find.text(''));
      await driver.enterText('test@test.com');
      await driver.waitFor(find.text('test@test.com'));
    });

    test('Test phone field',() async {
      await driver.tap(fieldFindPhone);
      await driver.waitFor(find.text(''));
      await driver.enterText('8888888888');
      await driver.waitFor(find.text('8888888888'));

    });

    test('Test submit field',() async {
      await driver.tap(fieldFindSubmit);
    });

    test('Success text', () async {
      final success = find.text('Вы успешно зарегистрировались');
      expect(await driver.getText(success), 'Вы успешно зарегистрировались');
    });
  });
}
