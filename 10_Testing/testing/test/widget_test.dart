// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:testing/views/login_view.dart';

import '../lib/components/register_form.dart';

void main() {
  group('Login view tests', () {
    testWidgets("test description", (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: LoginView(),
        localizationsDelegates: [
          DefaultMaterialLocalizations.delegate,
          DefaultWidgetsLocalizations.delegate,
        ],
      ));

      expect(find.byKey(Key('fieldEmail')), findsOneWidget);
      expect(find.byKey(Key('fieldPhone')), findsOneWidget);
      expect(find.text('Отправить'), findsOneWidget);
      expect(find.text('Добро пожаловать'), findsNothing);

      await tester.enterText(find.byKey(Key('fieldEmail')), 'test@test.com');
      expect(find.text('test@test.com'), findsOneWidget);

      expect(find.text('Введите email'), findsNothing);
      expect(find.text('Поле email заполнено не корректно'), findsNothing);

      await tester.enterText(find.byKey(Key('fieldPhone')), '8888888888');
      expect(find.text('8888888888'), findsOneWidget);

      expect(find.text('Введите телефон'), findsNothing);

      await tester.tap(find.text('Отправить'));
      await tester.pump();
      expect(find.text('Добро пожаловать'), findsOneWidget);
    });
  });


  group('Register view tests', () {
    testWidgets("test description", (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: LoginView(),
        localizationsDelegates: [
          DefaultMaterialLocalizations.delegate,
          DefaultWidgetsLocalizations.delegate,
        ],
      ));

      expect(find.byKey(Key('enter')), findsOneWidget);

      await tester.tap(find.byKey(Key('enter')));
      await tester.pump();

      expect(find.byKey(Key('fieldFirstName')), findsOneWidget);
      expect(find.byKey(Key('fieldLastName')), findsOneWidget);
      expect(find.byKey(Key('fieldPhone')), findsOneWidget);
      expect(find.byKey(Key('fieldEmail')), findsOneWidget);
      expect(find.text('Отправить'), findsOneWidget);
      expect(find.text('Вы успешно зарегистрировались'), findsNothing);

      await tester.enterText(find.byKey(Key('fieldFirstName')), 'Alexey');
      expect(find.text('Alexey'), findsOneWidget);

      expect(find.text('Введите имя'), findsNothing);

      await tester.enterText(find.byKey(Key('fieldLastName')), 'Blyumenshteyn');
      expect(find.text('Blyumenshteyn'), findsOneWidget);

      expect(find.text('Введите фамилию'), findsNothing);

      await tester.enterText(find.byKey(Key('fieldEmail')), 'test@test.com');
      expect(find.text('test@test.com'), findsOneWidget);

      expect(find.text('Заполните поле email'), findsNothing);
      expect(find.text('Емейл не корректный'), findsNothing);

      await tester.enterText(find.byKey(Key('fieldPhone')), '8888888888');
      expect(find.text('8888888888'), findsOneWidget);

      expect(find.text('Введите телефон'), findsNothing);

      await tester.tap(find.text('Отправить'));
      await tester.pump();
      expect(find.text('Вы успешно зарегистрировались'), findsOneWidget);
    });
  });
}
