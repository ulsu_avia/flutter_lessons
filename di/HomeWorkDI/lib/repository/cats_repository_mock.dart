import 'dart:io';
import 'package:HomeWorkDI/repository/fetch_file.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:HomeWorkDI/model/cats_facts.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
import 'package:dio/dio.dart';
import 'cat_interface.dart';
import 'dart:math';

class MockCatsRepository extends CatsInterface {
  final Dio _client;
  final DioAdapter _clientAdapter;

  // String path =
  //     'https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=1';

  MockCatsRepository(this._client, this._clientAdapter);

  List<Cat> mockDataList;

  @override
  Future<Cat> getCatsList() async {
    try {

      // String path =
      //     'https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=1';
      //
      // dynamic mockDataFromFile = await fetchFileFromAssets('assets/data.txt');
      //
      // _clientAdapter
      //     .onGet(path)
      //     .reply(200, jsonDecode(mockDataFromFile))
      //     .onPost(path)
      //     .reply(200, jsonDecode(mockDataFromFile));

      final response = await _client.get(
          'https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=1');

      var data = response.data;

      mockDataList = data.map<Cat>((catItem) => Cat.fromJson(catItem)).toList();

      Random random = new Random();

      return mockDataList[random.nextInt(mockDataList.length - 1)];
    } on DioError catch (e) {
      print(e);
      throw Exception();
    }

  }

  @override
  void closeClient() {
    _client.close();
    _clientAdapter.close();
  }
}
