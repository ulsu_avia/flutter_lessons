import 'package:HomeWorkDI/model/cats_facts.dart';

abstract class CatsInterface {
  Future<Cat> getCatsList();
  void closeClient();
}