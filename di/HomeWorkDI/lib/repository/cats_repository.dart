import 'dart:io';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:HomeWorkDI/model/cats_facts.dart';
import 'package:dio/dio.dart';
import 'cat_interface.dart';

class CatsRepository extends CatsInterface {
  final Dio _client;

  CatsRepository(this._client);

  Cat cat;

  @override
  Future<Cat> getCatsList() async {
    try {
      final response = await _client.get(
          'https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=1');

      var data = response.data;

      //cat = data.map<Cat>((catItem) => Cat.fromJson(catItem)).toList();

      cat = Cat.fromJson(response.data);

      return cat;

    } on DioError catch (e) {
      print(e);
      throw Exception();
    }
  }


  @override
  void closeClient() {
    _client.close();
  }

}
