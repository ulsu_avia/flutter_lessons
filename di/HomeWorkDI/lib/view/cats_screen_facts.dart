import 'dart:async';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:HomeWorkDI/keep_data/cats_facts_keep_data.dart';
import 'package:connectivity/connectivity.dart';
import 'package:injector/injector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dio/dio.dart';
import '../repository/cats_repository.dart';
import 'package:HomeWorkDI/model/cats_facts.dart';
import '../repository/cats_repository_mock.dart';

enum NewsScreenStatus { loadingData, error, showingData }
enum NewsScreenConnectionStatus { connected, connectLost }

class CatsScreenState extends StatefulWidget {
  @override
  _CatsScreenStateState createState() => _CatsScreenStateState();
}

class _CatsScreenStateState extends State<CatsScreenState> {
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  Cat _cat;
  NewsScreenStatus _status = NewsScreenStatus.loadingData;
  NewsScreenConnectionStatus _connectionStatus =
      NewsScreenConnectionStatus.connected;

  Future<void> fetchData() async {
    try {
      var data;

      if (_connectionStatus == NewsScreenConnectionStatus.connectLost) {
        data =
            await Injector.appInstance.get<MockCatsRepository>().getCatsList();
      } else {
        data = await Injector.appInstance.get<CatsRepository>().getCatsList();
      }

      setState(() {
        _cat = data;
        _status = NewsScreenStatus.showingData;
      });
    } catch (e) {
      //print(e);
      setState(() {
        _status = NewsScreenStatus.error;
      });
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    if (!mounted) {
      return Future.value(null);
    }

    _updateConnectionStatus(result);

    fetchData();
  }

  Future<void> checkConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    if (!mounted) {
      return Future.value(null);
    }

    _updateConnectionStatus(result);

    fetchData();
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = NewsScreenConnectionStatus.connected;
        });
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = NewsScreenConnectionStatus.connected;
        });
        break;
      case ConnectivityResult.none:
        setState(() {
          _connectionStatus = NewsScreenConnectionStatus.connectLost;
        });
        break;
      default:
        setState(() {
          _connectionStatus = NewsScreenConnectionStatus.connectLost;
        });
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    // _connectivitySubscription =
    //     _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    // initConnectivity();

    Injector.appInstance.get<CatFactKeepData>().subscribeConnection();
    Injector.appInstance.get<CatFactKeepData>().initConnectivity();
  }

  @override
  void dispose() {
    //_connectivitySubscription.cancel();
    Injector.appInstance.get<CatFactKeepData>().cancleConnection();
    Injector.appInstance.get<MockCatsRepository>().closeClient();
    Injector.appInstance.get<CatsRepository>().closeClient();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CatFactKeepData>(
      builder: (context, vm, child) {
        //vm = Injector.appInstance.get<CatFactKeepData>();
        return Scaffold(
          appBar: AppBar(title: Text('Cat Facts')),
          body: Container(
              child: (vm.checkShowingDataStatus() || vm.checkErrorStatus())
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Text(
                            vm.getActualRepositoryType(),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Center(
                          child: Text(
                            vm.getCatText(),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        RaisedButton(
                            child: Text(vm.setButtonText()),
                            onPressed: () {
                              setState(() {
                                vm.setLoadingDataStatus();
                              });
                              vm.fetchData();
                            }),
                      ],
                    )
                  : Center(
                      child: CircularProgressIndicator(),
                    )),
        );
      },
    );
  }
}
