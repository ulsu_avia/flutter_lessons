import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:injector/injector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dio/dio.dart';
import '../repository/cats_repository.dart';
import '../repository/cats_repository_mock.dart';
import 'package:HomeWorkDI/model/cats_facts.dart';

enum NewsScreenStatus { loadingData, error, showingData }
enum NewsScreenConnectionStatus { connected, connectLost }

class CatFactKeepData extends ChangeNotifier {
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  Cat _cat;
  NewsScreenStatus _status = NewsScreenStatus.loadingData;
  NewsScreenConnectionStatus _connectionStatus =
      NewsScreenConnectionStatus.connected;

  void subscribeConnection() {
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  bool checkShowingDataStatus() {
    return (_status == NewsScreenStatus.showingData);
  }

  bool checkLoadingDataStatus() {
    return (_status == NewsScreenStatus.loadingData);
  }

  bool checkErrorStatus() {
    return (_status == NewsScreenStatus.error);
  }

  void cancleConnection() {
    _connectivitySubscription.cancel();
  }

  bool setLoadingDataStatus() {
    _status = NewsScreenStatus.loadingData;
    notifyListeners();
  }

  bool setErrorStatus() {
    _status = NewsScreenStatus.error;
    notifyListeners();
  }

  bool setShowingDataStatus() {
    _status = NewsScreenStatus.showingData;
    notifyListeners();
  }

  bool checkConnectedStatus() {
    return (_connectionStatus == NewsScreenConnectionStatus.connected);
  }

  bool setConnectedStatus() {
    _connectionStatus = NewsScreenConnectionStatus.connected;
    notifyListeners();
  }

  bool setConnectedLostStatus() {
    _connectionStatus = NewsScreenConnectionStatus.connectLost;
    notifyListeners();
  }

  String getCatText() {
    if (checkShowingDataStatus()) {
      return _cat.text;
    } else {
      return 'Failed to get data';
    }
  }

  String getActualRepositoryType() {
    if(checkConnectedStatus())
      return Injector.appInstance.get<CatsRepository>().runtimeType.toString();
    else
      return Injector.appInstance.get<MockCatsRepository>().runtimeType.toString();
  }

  String setButtonText() {
    if (checkShowingDataStatus()) {
      return 'Show a new one';
    } else {
      return 'Repeat';
    }
  }

  Future<void> fetchData() async {
    try {
      var data;

      if (_connectionStatus == NewsScreenConnectionStatus.connectLost) {
        data =
            await Injector.appInstance.get<MockCatsRepository>().getCatsList();
      } else {
        data = await Injector.appInstance.get<CatsRepository>().getCatsList();
      }

      _cat = data;
      setShowingDataStatus();
    } catch (e) {
      //print(e);
      setErrorStatus();
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    _updateConnectionStatus(result);

    fetchData();
  }

  Future<void> checkConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    _updateConnectionStatus(result);

    fetchData();
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setConnectedStatus();
        break;
      case ConnectivityResult.mobile:
        setConnectedStatus();
        break;
      case ConnectivityResult.none:
        setConnectedLostStatus();
        break;
      default:
        setConnectedLostStatus();
        break;
    }
  }
}
