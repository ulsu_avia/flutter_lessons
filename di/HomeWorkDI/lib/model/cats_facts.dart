import 'dart:ffi';
import 'package:json_annotation/json_annotation.dart';
part 'cats_facts.g.dart';

@JsonSerializable(explicitToJson: true)
class Cat {
  final CatStatus status;
  final String type;
  final bool deleted;
  @JsonKey(name: '_id')
  final String id;
  @JsonKey(name: '__v')
  final int v;
  final String text;
  final String source;
  final String updatedAt;
  final String createdAt;
  final bool used;
  final String user;

  Cat({this.status, this.type, this.deleted, this.id, this.v, this.text,this.source, this.updatedAt,this.createdAt, this.used, this.user});

  factory Cat.fromJson(Map<String, dynamic> json) =>
      _$CatFromJson(json);
  Map<String, dynamic> toJson() => _$CatToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CatStatus {
  @JsonKey(defaultValue: false)
  final bool verified;
  final int sentCount;

  CatStatus({this.verified, this.sentCount});

  factory CatStatus.fromJson(Map<String, dynamic> json) =>
      _$CatStatusFromJson(json);
  Map<String, dynamic> toJson() => _$CatStatusToJson(this);
}
