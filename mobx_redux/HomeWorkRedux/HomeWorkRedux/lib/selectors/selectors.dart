// Project imports:
import 'package:HomeWorkRedux/core/optional.dart';
import 'package:HomeWorkRedux/model/models.dart';
import 'package:HomeWorkRedux/model/todo.dart';

List<Todo> todosSelector(AppState state) => state.todos;

Optional<Todo> todoSelector(List<Todo> todos, String id) {
  try {
    return Optional.of(todos.firstWhere((todo) => todo.id == id));
  } catch (e) {
    return Optional.absent();
  }
}
