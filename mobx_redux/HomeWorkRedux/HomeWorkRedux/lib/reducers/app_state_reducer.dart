// Project imports:
import 'package:HomeWorkRedux/model/models.dart';
import 'package:HomeWorkRedux/reducers/loading_reducer.dart';
import 'package:HomeWorkRedux/reducers/todos_reducer.dart';

// We create the State reducer by combining many smaller reducers into one!
AppState appReducer(AppState state, action) {
  return AppState(
    isLoading: loadingReducer(state.isLoading, action),
    todos: todosReducer(state.todos, action),
  );
}
