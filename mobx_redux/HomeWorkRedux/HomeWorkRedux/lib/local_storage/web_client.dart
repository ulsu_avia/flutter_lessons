// Dart imports:
import 'dart:async';

// Project imports:
import 'package:HomeWorkRedux/core/todo_entity.dart';
import 'package:HomeWorkRedux/core/todos_repository.dart';

class WebClient implements TodosRepository {
  final Duration delay;

  const WebClient([this.delay = const Duration(milliseconds: 3000)]);

  @override
  Future<List<TodoEntity>> loadTodos() async {
    return Future.delayed(
        delay,
        () => []);
  }

  @override
  Future<bool> saveTodos(List<TodoEntity> todos) async {
    return Future.value(true);
  }
}
