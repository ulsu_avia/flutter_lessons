// Dart imports:
import 'dart:async';
import 'dart:core';

// Package imports:
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

// Project imports:
import 'package:HomeWorkRedux/core/todo_entity.dart';
import 'package:HomeWorkRedux/core/todos_repository.dart';
import 'package:HomeWorkRedux/local_storage/web_client.dart';

class LocalStorageRepository implements TodosRepository {
  final TodosRepository localStorage;
  final TodosRepository webClient;

  const LocalStorageRepository({
    @required this.localStorage,
    this.webClient = const WebClient(),
  });

  @override
  Future<List<TodoEntity>> loadTodos() async {
    try {
      return await localStorage.loadTodos();
    } catch (e) {
      final todos = await webClient.loadTodos();

      await localStorage.saveTodos(todos);

      todos.sort((a, b) => DateFormat('dd.MM.yy hh:mm:ss').parse(b.date).compareTo(DateFormat('dd.MM.yy kk:mm:ss').parse(a.date)));

      return todos;
    }
  }

  // Persists todos to local disk and the web
  @override
  Future saveTodos(List<TodoEntity> todos) {
    todos.sort((a, b) => DateFormat('dd.MM.yy hh:mm:ss').parse(b.date).compareTo(DateFormat('dd.MM.yy kk:mm:ss').parse(a.date)));

    return Future.wait<dynamic>([
      localStorage.saveTodos(todos),
      webClient.saveTodos(todos),
    ]);
  }
}
