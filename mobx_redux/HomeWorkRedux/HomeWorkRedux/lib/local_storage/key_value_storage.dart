// Dart imports:
import 'dart:convert';

// Package imports:
import 'package:intl/intl.dart';
import 'package:key_value_store/key_value_store.dart';

// Project imports:
import 'package:HomeWorkRedux/core/todo_entity.dart';
import 'package:HomeWorkRedux/core/todos_repository.dart';

class KeyValueStorage implements TodosRepository {
  final String key;
  final KeyValueStore store;
  final JsonCodec codec;

  const KeyValueStorage(this.key, this.store, [this.codec = json]);

  @override
  Future<List<TodoEntity>> loadTodos() async {
    return codec
        .decode(store.getString(key))['todos']
        .cast<Map<String, Object>>()
        .map<TodoEntity>(TodoEntity.fromJson)
        .toList(growable: false);
  }

  @override
  Future<bool> saveTodos(List<TodoEntity> todos) {
    //todos.sort((a, b) => DateFormat('dd.MM.yy kk:mm').parse(b.date).compareTo(DateFormat('dd.MM.yy kk:mm').parse(a.date)));
    return store.setString(
      key,
      codec.encode({
        'todos': todos.map((todo) => todo.toJson()).toList(),
      }),
    );
  }
}
