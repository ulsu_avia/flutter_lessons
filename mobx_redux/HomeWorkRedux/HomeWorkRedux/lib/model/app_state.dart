
import 'package:meta/meta.dart';

// Project imports:
import 'package:HomeWorkRedux/model/models.dart';

@immutable
class AppState {
  final bool isLoading;
  final List<Todo> todos;

  AppState(
      {this.isLoading = false,
      this.todos = const []});

  factory AppState.loading() => AppState(isLoading: true);

  AppState copyWith({
    bool isLoading,
    List<Todo> todos,
  }) {
    return AppState(
      isLoading: isLoading ?? this.isLoading,
      todos: todos ?? this.todos,
    );
  }

  @override
  int get hashCode =>
      isLoading.hashCode ^
      todos.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState &&
          runtimeType == other.runtimeType &&
          isLoading == other.isLoading &&
          todos == other.todos;

  @override
  String toString() {
    return 'AppState{isLoading: $isLoading, todos: $todos}';
  }
}
