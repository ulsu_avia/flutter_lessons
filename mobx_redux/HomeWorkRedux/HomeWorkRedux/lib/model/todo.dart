

// Package imports:
import 'package:meta/meta.dart';

// Project imports:
import 'package:HomeWorkRedux/core/todo_entity.dart';
import 'package:HomeWorkRedux/core/uuid.dart';

@immutable
class Todo {
  final String task;
  final String note;
  final String date;
  final String id;

  Todo(this.task, {String note = '', String date = '', String id})
      : note = note ?? '',
        date = date ?? '',
        id = id ?? Uuid().generateV4();

  Todo copyWith({String task, String note, String date, String id}) {
    return Todo(
      task ?? this.task,
      note: note ?? this.note,
      date: date ?? this.date,
      id: id ?? this.id,
    );
  }

  @override
  int get hashCode =>
      task.hashCode ^ note.hashCode ^ date.hashCode ^ id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Todo &&
          runtimeType == other.runtimeType &&
          task == other.task &&
          note == other.note &&
          date == other.date &&
          id == other.id;

  @override
  String toString() {
    return 'Todo{task: $task, note: $note, date: $date, id: $id}';
  }

  TodoEntity toEntity() {
    return TodoEntity(task, note, date, id);
  }

  static Todo fromEntity(TodoEntity entity) {
    return Todo(
      entity.task,
      note: entity.note,
      date: entity.date,
      id: entity.id ?? Uuid().generateV4(),
    );
  }
}
