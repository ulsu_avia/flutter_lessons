
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

// Package imports:
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

// Project imports:
import 'package:HomeWorkRedux/actions/actions.dart';
import 'package:HomeWorkRedux/core/localization.dart';
import 'package:HomeWorkRedux/model/models.dart';
import 'package:HomeWorkRedux/presentation/todo_list.dart';

class FilteredTodos extends StatelessWidget {
  FilteredTodos({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, vm) {
        return (vm.todos.length > 0)
            ? TodoList(
                todos: vm.todos,
                onRemove: vm.onRemove,
                onUndoRemove: vm.onUndoRemove,
              )
            : Center(
                child: Text(
                  ArchSampleLocalizations.of(context).nonotes,
                ),
              );
      },
    );
  }
}

class _ViewModel {
  final List<Todo> todos;
  final bool loading;
  final Function(Todo) onRemove;
  final Function(Todo) onUndoRemove;

  _ViewModel({
    @required this.todos,
    @required this.loading,
    @required this.onRemove,
    @required this.onUndoRemove,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      todos: store.state.todos,
      loading: store.state.isLoading,
      onRemove: (todo) {
        store.dispatch(DeleteTodoAction(todo.id));
      },
      onUndoRemove: (todo) {
        store.dispatch(AddTodoAction(todo));
      },
    );
  }
}
