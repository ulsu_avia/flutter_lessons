// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

// Project imports:
import 'package:HomeWorkRedux/actions/actions.dart';
import 'package:HomeWorkRedux/containers/add_todo.dart';
import 'package:HomeWorkRedux/core/localization.dart';
import 'package:HomeWorkRedux/core/routes.dart';
import 'package:HomeWorkRedux/localization.dart';
import 'package:HomeWorkRedux/model/models.dart';
import 'package:HomeWorkRedux/presentation/home_screen.dart';

class ReduxApp extends StatelessWidget {
  final Store<AppState> store;

  const ReduxApp({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        onGenerateTitle: (context) => ReduxLocalizations.of(context).appTitle,
        //theme: ArchSampleTheme.theme,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          ArchSampleLocalizationsDelegate(),
          ReduxLocalizationsDelegate(),
        ],
        supportedLocales: [
          const Locale('ru', 'RU'), // English, no country code
        ],
        //theme: ArchSampleTheme.theme,
        routes: {
          ArchSampleRoutes.home: (context) {
            return HomeScreen(
              onInit: () {
                StoreProvider.of<AppState>(context).dispatch(LoadTodosAction());
              },
            );
          },
          ArchSampleRoutes.addTodo: (context) {
            return AddTodo();
          },
        },
      ),
    );
  }
}
