// Package imports:
import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent = void Function(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  @override
  String get localeName => 'en';

  static String m0(task) => 'Deleted "${task}"';

  @override
  final messages = _notInlinedMessages(_notInlinedMessages);

  static Map<String, dynamic> _notInlinedMessages(_) => {
        'addTodo': MessageLookupByLibrary.simpleMessage('Add Todo'),
        'cancel': MessageLookupByLibrary.simpleMessage('Cancel'),
        'delete': MessageLookupByLibrary.simpleMessage('Delete'),
        'deleteTodo': MessageLookupByLibrary.simpleMessage('Delete Todo'),
        'deleteTodoConfirmation':
            MessageLookupByLibrary.simpleMessage('Delete this todo?'),
        'editTodo': MessageLookupByLibrary.simpleMessage('Edit Todo'),
        'emptyTodoError':
            MessageLookupByLibrary.simpleMessage('Please enter some text'),
        'newTodoHint':
            MessageLookupByLibrary.simpleMessage('Title'),
        'notesHint':
            MessageLookupByLibrary.simpleMessage('Description'),
        'saveChanges': MessageLookupByLibrary.simpleMessage('Save changes'),
        'todoDeleted': m0,
        'todoDetails': MessageLookupByLibrary.simpleMessage('Todo Details'),
        'todos': MessageLookupByLibrary.simpleMessage('Todos'),
        'undo': MessageLookupByLibrary.simpleMessage('Undo'),
        'notes': MessageLookupByLibrary.simpleMessage('Notes'),
        'note': MessageLookupByLibrary.simpleMessage('Note'),
        'nonotes': MessageLookupByLibrary.simpleMessage('No notes'),
        'deleteMessage': MessageLookupByLibrary.simpleMessage('Delete note?'),
        'yes': MessageLookupByLibrary.simpleMessage('Yes'),
        'no': MessageLookupByLibrary.simpleMessage('No')
      };
}
