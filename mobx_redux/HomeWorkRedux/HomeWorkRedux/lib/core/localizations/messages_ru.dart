// Package imports:
import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent = void Function(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  @override
  String get localeName => 'ru';

  static String m0(task) => 'Deleted "${task}"';

  @override
  final messages = _notInlinedMessages(_notInlinedMessages);

  static Map<String, dynamic> _notInlinedMessages(_) => {
        'addTodo': MessageLookupByLibrary.simpleMessage('Добавить задачу'),
        'cancel': MessageLookupByLibrary.simpleMessage('Отмена'),
        'delete': MessageLookupByLibrary.simpleMessage('Удалить'),
        'deleteTodo': MessageLookupByLibrary.simpleMessage('Удалить задачу'),
        'deleteTodoConfirmation':
            MessageLookupByLibrary.simpleMessage('Удалить задачу?'),
        'editTodo': MessageLookupByLibrary.simpleMessage('Редактировать задачу'),
        'emptyTodoError':
            MessageLookupByLibrary.simpleMessage('Введите, пожалуйста, текст'),
        'newTodoHint':
            MessageLookupByLibrary.simpleMessage('Заголовок'),
        'notesHint':
            MessageLookupByLibrary.simpleMessage('Описание'),
        'saveChanges': MessageLookupByLibrary.simpleMessage('Сохранить изменения'),
        'todoDeleted': m0,
        'todoDetails': MessageLookupByLibrary.simpleMessage('Детали задачи'),
        'todos': MessageLookupByLibrary.simpleMessage('Задачи'),
        'undo': MessageLookupByLibrary.simpleMessage('Отмена'),
        'notes': MessageLookupByLibrary.simpleMessage('Заметки'),
        'note': MessageLookupByLibrary.simpleMessage('Заметка'),
        'nonotes': MessageLookupByLibrary.simpleMessage('Заметок нет'),
        'deleteMessage': MessageLookupByLibrary.simpleMessage('Удалить заметку?'),
        'yes': MessageLookupByLibrary.simpleMessage('Да'),
        'no': MessageLookupByLibrary.simpleMessage('Нет')
      };
}
