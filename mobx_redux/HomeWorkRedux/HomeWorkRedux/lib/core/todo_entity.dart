
class TodoEntity {
  final String task;
  final String note;
  final String date;
  final String id;

  TodoEntity(this.task, this.note, this.date, this.id);

  @override
  int get hashCode =>
     task.hashCode ^ note.hashCode ^ date.hashCode ^ id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TodoEntity &&
          runtimeType == other.runtimeType &&
          task == other.task &&
          note == other.note &&
          date == other.date &&
          id == other.id;

  Map<String, Object> toJson() {
    return {
      'task': task,
      'note': note,
      'date': date,
      'id': id,
    };
  }

  @override
  String toString() {
    return 'TodoEntity{task: $task, note: $note, date: $date, id: $id}';
  }

  static TodoEntity fromJson(Map<String, Object> json) {
    return TodoEntity(
      json['task'] as String,
      json['note'] as String,
      json['date'] as String,
      json['id'] as String,
    );
  }
}
