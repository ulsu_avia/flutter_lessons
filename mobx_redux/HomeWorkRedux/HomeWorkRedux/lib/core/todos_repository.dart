
import 'dart:async';
import 'dart:core';

// Project imports:
import 'todo_entity.dart';

abstract class TodosRepository {
  Future<List<TodoEntity>> loadTodos();

  // Persists todos to local disk and the web
  Future saveTodos(List<TodoEntity> todos);
}
