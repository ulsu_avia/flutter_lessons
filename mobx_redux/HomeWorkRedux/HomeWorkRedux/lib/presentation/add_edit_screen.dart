

// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// Package imports:
import 'package:flutter_redux/flutter_redux.dart';

// Project imports:
import 'package:HomeWorkRedux/actions/actions.dart';
import 'package:HomeWorkRedux/containers/alert_dialog.dart';
import 'package:HomeWorkRedux/core/keys.dart';
import 'package:HomeWorkRedux/core/localization.dart';
import 'package:HomeWorkRedux/model/models.dart';

typedef OnSaveCallback = void Function(String task, String note);

class AddEditScreen extends StatefulWidget {
  final bool isEditing;
  final OnSaveCallback onSave;
  final Todo todo;

  AddEditScreen(
      {Key key, @required this.onSave, @required this.isEditing, this.todo})
      : super(key: key ?? ArchSampleKeys.addTodoScreen);
  @override
  _AddEditScreenState createState() => _AddEditScreenState();
}

class _AddEditScreenState extends State<AddEditScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _task;
  String _note;

  bool get isEditing => widget.isEditing;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return WillPopScope(
      onWillPop: () {
        if (_formKey.currentState.validate()) {
          _formKey.currentState.save();
          widget.onSave(_task, _note);
        }
        Navigator.pop(context);
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                widget.onSave(_task, _note);
              }
              Navigator.pop(context);
            },
          ),
          title: Text(
            ArchSampleLocalizations.of(context).note,
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    final store = StoreProvider.of<AppState>(context);

                    var dialog = CustomAlertDialog(
                            title: ArchSampleLocalizations.of(context)
                                .deleteMessage,
                            message: "",
                            onPostivePressed: () {
                              if (widget?.todo?.id != null) {
                                store.dispatch(
                                  DeleteTodoAction(widget.todo.id),
                                );
                              };
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                            },
                            positiveBtnText:
                                ArchSampleLocalizations.of(context).yes,
                            negativeBtnText:
                                ArchSampleLocalizations.of(context).no);
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => dialog);
                  },
                  child: Icon(
                    Icons.delete,
                  ),
                )),
          ],
        ),
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                TextFormField(
                  initialValue: isEditing ? widget.todo.task : '',
                  key: ArchSampleKeys.taskField,
                  autofocus: !isEditing,
                  style: textTheme.headline,
                  decoration: InputDecoration(
                    hintText: ArchSampleLocalizations.of(context).newTodoHint,
                  ),
                  validator: (val) {
                    return val.trim().isEmpty
                        ? ArchSampleLocalizations.of(context).emptyTodoError
                        : null;
                  },
                  onSaved: (value) => _task = value,
                ),
                TextFormField(
                  initialValue: isEditing ? widget.todo.note : '',
                  key: ArchSampleKeys.noteField,
                  maxLines: 10,
                  style: textTheme.subhead,
                  decoration: InputDecoration(
                    hintText: ArchSampleLocalizations.of(context).notesHint,
                  ),
                  onSaved: (value) => _note = value,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
