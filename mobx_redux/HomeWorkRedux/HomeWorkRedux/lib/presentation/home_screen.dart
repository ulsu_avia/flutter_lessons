

// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:HomeWorkRedux/containers/todos.dart';
import 'package:HomeWorkRedux/core/keys.dart';
import 'package:HomeWorkRedux/core/localization.dart';
import 'package:HomeWorkRedux/core/routes.dart';
import 'package:HomeWorkRedux/model/models.dart';

class HomeScreen extends StatefulWidget {
  final void Function() onInit;

  HomeScreen({@required this.onInit}) : super(key: ArchSampleKeys.homeScreen);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    widget.onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ArchSampleLocalizations.of(context).notes),
      ),
      body:
          FilteredTodos(),
      floatingActionButton: FloatingActionButton(
        key: ArchSampleKeys.addTodoFab,
        onPressed: () {
          Navigator.pushNamed(context, ArchSampleRoutes.addTodo);
        },
        child: Icon(Icons.add),
        tooltip: ArchSampleLocalizations.of(context).addTodo,
      ),
    );
  }
}
