// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// Project imports:
import 'package:HomeWorkRedux/containers/alert_dialog.dart';
import 'package:HomeWorkRedux/core/keys.dart';
import 'package:HomeWorkRedux/core/localization.dart';
import 'package:HomeWorkRedux/model/models.dart';

class TodoItem extends StatelessWidget {
  final DismissDirectionCallback onDismissed;
  final GestureTapCallback onTap;
  final Todo todo;
  final Function(Todo) onRemove;

  TodoItem({
    @required this.onDismissed,
    @required this.onTap,
    @required this.todo,
    @required this.onRemove,
  });

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ArchSampleKeys.todoItem(todo.id),
      onDismissed: onDismissed,
      child: ListTile(
        onTap: onTap,
        // leading: Checkbox(
        //   key: ArchSampleKeys.todoItemCheckbox(todo.id),
        //   value: todo.complete,
        // ),
        title: Hero(
          tag: '${todo.id}__heroTag',
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Text(
              todo.task,
              key: ArchSampleKeys.todoItemTask(todo.id),
              style: Theme.of(context).textTheme.title,
            ),
          ),
        ),
        subtitle: Text(
          "${todo.note}",
          key: ArchSampleKeys.todoItemNote(todo.id),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.subhead,
        ),
        trailing: InkWell(
          onTap: () {
            var dialog = CustomAlertDialog(
                title: ArchSampleLocalizations.of(context).deleteMessage,
                message: "",
                onPostivePressed: () {
                  onRemove(todo);
                  Navigator.of(context).pop();
                },
                positiveBtnText: ArchSampleLocalizations.of(context).yes,
                negativeBtnText: ArchSampleLocalizations.of(context).no);
            showDialog(
                context: context, builder: (BuildContext context) => dialog);
          }, // button pressed
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.delete,
              ), // icon
              Text("${todo.date}"), // text
            ],
          ),
        ),
      ),
    );
  }
}
