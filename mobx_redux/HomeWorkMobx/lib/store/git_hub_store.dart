// Package imports:
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:mobx/mobx.dart';

// Project imports:
import 'package:HomeWorkMobx/model/git_hub_jobs.dart';

part 'git_hub_store.g.dart';

class GitHubStore = _GitHubStore with _$GitHubStore;

enum GitJobsStatus {initial, loading, success, failure }

enum ConnectionStatus { connected, connectLost }

abstract class _GitHubStore with Store {
  final Dio client = Dio();

  @observable
  GitJobsStatus status = GitJobsStatus.initial;

  @observable
  ConnectionStatus conStatus = ConnectionStatus.connected;

  // No need to observe this as we are relying on the fetchReposFuture.status
  @observable
  List<Jobs> repositories = [];

  // No need to observe this as we are relying on the fetchReposFuture.status
  @observable
  Jobs actualItem;

  // We are starting with an empty future to avoid a null check
  // @observable
  // List<Jobs> fetchReposFuture = emptyResponse;

  @observable
  String job = '';

  @computed
  bool get hasResults =>
      repositories != emptyResponse;
          // &&
          // fetchReposFuture.status == FutureStatus.fulfilled;

  static List<Jobs> emptyResponse = [];
  //ObservableFuture.value([]);

  @action
  Future<List<Jobs>> getJobsList() async {
    try {

      await checkConnectivity();


      if(conStatus == ConnectionStatus.connected) {
        status = GitJobsStatus.loading;
        final response = await client.get(
            'https://jobs.github.com/positions.json?search=${job}');

        if (response.statusCode == 200) {
          var data = response.data;

          repositories =
              data.map<Jobs>((jobItem) => Jobs.fromJson(jobItem)).toList();

          status = GitJobsStatus.success;
          return repositories;
        }
        throw Exception('error fetching posts');
      }else{
        status = GitJobsStatus.failure;
      }

    } on DioError catch (e) {
      print(e);
      status = GitJobsStatus.failure;
      throw Exception();
    }
  }

  @action
  void setJobs(String text) {
    repositories = emptyResponse;
    job = text;
  }

  final Connectivity _connectivity = Connectivity();


  Future<void> checkConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on Exception catch (e) {
      print(e.toString());
    }

    _updateConnectionStatus(result);
  }

  bool checkConnectedStatus() {
    checkConnectivity();
    return (conStatus == ConnectionStatus.connected);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        conStatus = ConnectionStatus.connected;
        break;
      case ConnectivityResult.mobile:
        conStatus = ConnectionStatus.connected;
        break;
      case ConnectivityResult.none:
        conStatus = ConnectionStatus.connectLost;
        break;
      default:
        conStatus = ConnectionStatus.connectLost;
        break;
    }
  }

}
