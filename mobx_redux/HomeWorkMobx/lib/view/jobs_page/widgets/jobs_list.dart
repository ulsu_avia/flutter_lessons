// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_mobx/flutter_mobx.dart';

// Project imports:
import 'package:HomeWorkMobx/store/git_hub_store.dart';
import 'package:HomeWorkMobx/widgets/bottom_loader.dart';
import 'package:HomeWorkMobx/widgets/failure.dart';
import 'jobs_items.dart';

class JobsList extends StatelessWidget {
  const JobsList(this.store);

  final GitHubStore store;

  @override
  Widget build(BuildContext context) => Observer(builder: (_) {
        switch (store.status) {
          case GitJobsStatus.initial:
            return const Center(child: Text('Введите описание вакансии'));
          case GitJobsStatus.failure:
            return Failure(store);
          case GitJobsStatus.success:
            if (store.repositories.isEmpty) {
              return const Center(child: Text('Нет записей'));
            }
            return ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return index >= store.repositories.length
                    ? BottomLoader()
                    : JobsListItem(
                        jobItem: store.repositories.elementAt(index),
                      );
              },
              itemCount: store.repositories.length,
            );
          default:
            return Center(child: CircularProgressIndicator());
        }
      });
}
