// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:HomeWorkMobx/store/git_hub_store.dart';

class UserInput extends StatelessWidget {
  const UserInput(this.store);

  final GitHubStore store;

  @override
  Widget build(BuildContext context) => Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: TextField(
                autocorrect: false,
                autofocus: true,
                onSubmitted: (String job) {
                  store.setJobs(job);

                  // ignore: cascade_invocations
                  store.getJobsList();
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: RaisedButton(
              onPressed: () {
                onPressed: store.getJobsList();
              },
              shape: RoundedRectangleBorder(
                borderRadius:
                    const BorderRadius.all(const Radius.circular(2.0)),
              ),
              child: Text(
                "Искать",
              ),
            ),
          )
        ],
      );
}
