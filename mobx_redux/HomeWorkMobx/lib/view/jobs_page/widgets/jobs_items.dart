// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:HomeWorkMobx/model/git_hub_jobs.dart';

class JobsListItem extends StatelessWidget {
  const JobsListItem({Key key, @required this.jobItem})
      : super(key: key);

  final Jobs jobItem;


  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Card(
      child: ListTile(
        title: Text(jobItem.company),
        subtitle: Text(jobItem.location),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(jobItem.created_at),
          ],
        ),
        onTap: () {
          Navigator.of(context).pushNamed('/info', arguments: {'jobItem': jobItem,});
        },
      ),
    );
  }
}
