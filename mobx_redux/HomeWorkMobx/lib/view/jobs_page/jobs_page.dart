// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:HomeWorkMobx/store/git_hub_store.dart';
import 'widgets/jobs_list.dart';
import 'widgets/user_input.dart';

class JobsPage extends StatelessWidget {
  static const routeName = '/';

  final GitHubStore store = GitHubStore();

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: const Text('Github Repos'),
      ),
      body:  Column(
          children: <Widget>[
            Container(
                padding: const EdgeInsets.only(bottom: 10),
                child: UserInput(store)),
            Expanded(child: JobsList(store)),
          ],
        ),
      );
}
