// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_html/flutter_html.dart';

// Project imports:
import 'package:HomeWorkMobx/model/git_hub_jobs.dart';

class JobsInfoPage extends StatelessWidget {
  const JobsInfoPage([this.jobItem]);
  static const routeName = '/info';

  final Jobs jobItem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${jobItem.title}'),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate([
              jobItem.company_logo != null ? Padding(
                padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                child: Container(
                  padding: EdgeInsets.all(10),
                  height: 250,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.contain,
                      image: NetworkImage(jobItem.company_logo),
                    ),
                  ),
                ),
              ) : Container(),
              Container(
                padding: EdgeInsets.only(top: 5, right: 5, left: 5),
                child: Text(
                  jobItem.title,
                  style: TextStyle(color: Colors.black, fontSize: 25),
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.ltr,
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 5, right: 7, left: 7),
                child: Text(
                  'company: ${jobItem.company}',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.left,
                  textDirection: TextDirection.ltr,
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 7, left: 7),
                child: Text(
                  'location: ${jobItem.location}',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.left,
                  textDirection: TextDirection.ltr,
                ),
              ),
              Container(
                child: Html(
                  data: jobItem.description,
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
