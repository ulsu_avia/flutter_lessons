// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:HomeWorkMobx/store/git_hub_store.dart';

class Failure extends StatelessWidget {
  const Failure(this.store);

  final GitHubStore store;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(child: Text('Не удалось получить данные')),
          RaisedButton(
              child: Text('Повторить'),
              onPressed: () {
                store.getJobsList();
              }),
        ],
      ),
    );
  }
}
