// Package imports:
import 'package:json_annotation/json_annotation.dart';

part 'git_hub_jobs.g.dart';

@JsonSerializable(explicitToJson: true)
class Jobs {
  final String id;
  final String type;
  final String url;
  final String created_at;
  final String company;
  final String company_url;
  final String location;
  final String title;
  final String description;
  final String how_to_apply;
  final String company_logo;
  //final List<String> subBreeds;

  Jobs(
      {this.id,
      this.type,
      this.url,
      this.created_at,
      this.company,
      this.company_url,
      this.location,
      this.title,
      this.description,
      this.how_to_apply,
      this.company_logo});

  factory Jobs.fromJson(Map<String, dynamic> json) => _$JobsFromJson(json);
  Map<String, dynamic> toJson() => _$JobsToJson(this);
}
