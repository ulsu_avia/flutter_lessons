import 'dart:ffi';

import 'package:json_annotation/json_annotation.dart';

part 'hotel.g.dart';

@JsonSerializable(explicitToJson: true)
class HotelPreview {
  final String uuid;
  final String name;
  final String poster;

  HotelPreview({this.uuid, this.name, this.poster});

  factory HotelPreview.fromJson(Map<String, dynamic> json) =>
      _$HotelPreviewFromJson(json);
  Map<String, dynamic> toJson() => _$HotelPreviewToJson(this);
}

@JsonSerializable(explicitToJson: true)
class HotelInfoPreview {
  final String uuid;
  final String name;
  final String poster;
  final double price;
  final double rating;
  @JsonKey(defaultValue: 'n/a')
  final HotelAddress address;
  final HotelServices services;
  final List<String> photos;

  HotelInfoPreview(
      {this.uuid,
      this.name,
      this.poster,
      this.price,
      this.rating,
      this.address,
        this.services,
      this.photos});

  factory HotelInfoPreview.fromJson(Map<String, dynamic> json) =>
      _$HotelInfoPreviewFromJson(json);
  Map<String, dynamic> toJson() => _$HotelInfoPreviewToJson(this);
}

@JsonSerializable(explicitToJson: true)
class HotelCoords {
  final String lat;
  final String lan;

  HotelCoords({this.lat, this.lan});

  factory HotelCoords.fromJson(Map<String, dynamic> json) =>
      _$HotelCoordsFromJson(json);
  Map<String, dynamic> toJson() => _$HotelCoordsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class HotelAddress {
  @JsonKey(defaultValue: 'n/a')
  final String country;
  @JsonKey(defaultValue: 'n/a')
  final String street;
  @JsonKey(defaultValue: 'n/a')
  final String city;
  @JsonKey(name: 'zip_code')
  final int zipcode;
  //final HotelCoords coords;

  HotelAddress({
    this.country,
    this.street,
    this.city,
    this.zipcode,
    //this.coords
  });

  factory HotelAddress.fromJson(Map<String, dynamic> json) =>
      _$HotelAddressFromJson(json);
  Map<String, dynamic> toJson() => _$HotelAddressToJson(this);
}

@JsonSerializable(explicitToJson: true)
class HotelServices {
  final List<String> free;
  final List<String> paid;

  HotelServices({this.free, this.paid});

  factory HotelServices.fromJson(Map<String, dynamic> json) =>
      _$HotelServicesFromJson(json);
  Map<String, dynamic> toJson() => _$HotelServicesToJson(this);
}

@JsonSerializable(explicitToJson: true)
class HotelPhotos {
  final List<String> photos;

  HotelPhotos({this.photos});

  factory HotelPhotos.fromJson(Map<String, dynamic> json) =>
      _$HotelPhotosFromJson(json);
  Map<String, dynamic> toJson() => _$HotelPhotosToJson(this);
}
