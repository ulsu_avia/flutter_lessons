// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hotel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HotelPreview _$HotelPreviewFromJson(Map<String, dynamic> json) {
  return HotelPreview(
    uuid: json['uuid'] as String,
    name: json['name'] as String,
    poster: json['poster'] as String,
  );
}

Map<String, dynamic> _$HotelPreviewToJson(HotelPreview instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'name': instance.name,
      'poster': instance.poster,
    };

HotelInfoPreview _$HotelInfoPreviewFromJson(Map<String, dynamic> json) {
  return HotelInfoPreview(
    uuid: json['uuid'] as String,
    name: json['name'] as String,
    poster: json['poster'] as String,
    price: (json['price'] as num)?.toDouble(),
    rating: (json['rating'] as num)?.toDouble(),
    address: json['address'] == null
        ? null
        : HotelAddress.fromJson(json['address'] as Map<String, dynamic>) ??
            'n/a',
    services: json['services'] == null
        ? null
        : HotelServices.fromJson(json['services'] as Map<String, dynamic>),
    photos: (json['photos'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$HotelInfoPreviewToJson(HotelInfoPreview instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'name': instance.name,
      'poster': instance.poster,
      'price': instance.price,
      'rating': instance.rating,
      'address': instance.address?.toJson(),
      'services': instance.services?.toJson(),
      'photos': instance.photos,
    };

HotelCoords _$HotelCoordsFromJson(Map<String, dynamic> json) {
  return HotelCoords(
    lat: json['lat'] as String,
    lan: json['lan'] as String,
  );
}

Map<String, dynamic> _$HotelCoordsToJson(HotelCoords instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lan': instance.lan,
    };

HotelAddress _$HotelAddressFromJson(Map<String, dynamic> json) {
  return HotelAddress(
    country: json['country'] as String ?? 'n/a',
    street: json['street'] as String ?? 'n/a',
    city: json['city'] as String ?? 'n/a',
    zipcode: json['zip_code'] as int,
  );
}

Map<String, dynamic> _$HotelAddressToJson(HotelAddress instance) =>
    <String, dynamic>{
      'country': instance.country,
      'street': instance.street,
      'city': instance.city,
      'zip_code': instance.zipcode,
    };

HotelServices _$HotelServicesFromJson(Map<String, dynamic> json) {
  return HotelServices(
    free: (json['free'] as List)?.map((e) => e as String)?.toList(),
    paid: (json['paid'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$HotelServicesToJson(HotelServices instance) =>
    <String, dynamic>{
      'free': instance.free,
      'paid': instance.paid,
    };

HotelPhotos _$HotelPhotosFromJson(Map<String, dynamic> json) {
  return HotelPhotos(
    photos: (json['photos'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$HotelPhotosToJson(HotelPhotos instance) =>
    <String, dynamic>{
      'photos': instance.photos,
    };
