import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../models/hotel.dart';
import 'package:hotels/widgets/hotel_services.dart';
import 'package:hotels/widgets/hotel_info_text.dart';
import 'package:hotels/widgets/hotel_info_header.dart';
import 'package:hotels/widgets/hotel_info_slider.dart';

class HotelInfo extends StatefulWidget {
  static const routeName = '/hotelinfo';

  HotelInfo({Key key, this.uuid}) : super(key: key);

  final String uuid;

  @override
  _HotelInfoState createState() => _HotelInfoState();
}

class _HotelInfoState extends State<HotelInfo> {
  bool isLoad = false;
  bool hasError = false;
  String errorMessage;
  HotelInfoPreview hotelInfoPreview;
  Dio _dio = Dio();

  void initState() {
    super.initState();
    //getData();
    getDataDio();
  }

  getDataDio() async {
    setState(() {
      isLoad = true;
    });
    //print('${widget.uuid}');
    try {
      final response = await _dio.get('https://run.mocky.io/v3/${widget.uuid}');
      var data = response.data;
      //print(response);
      hotelInfoPreview = HotelInfoPreview.fromJson(response.data);
    } on DioError catch (e) {
      //print(e.response.data['message']);
      setState(() {
        errorMessage = e.response.data['message'];
        hasError = true;
        isLoad = false;
      });
    }

    setState(() {
      isLoad = false;
    });
  }

  goBack(BuildContext context) {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: true,
        title: Center(
          child: Container(
            padding: EdgeInsets.only(right: 45),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                if (!isLoad && hasError == false)
                  Text(
                    hotelInfoPreview.name,
                  ),
              ],
            ),
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_rounded),
          color: Colors.white,
          onPressed: () {
            goBack(context);
          },
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            if (isLoad)
              Center(
                child: CircularProgressIndicator(),
              ),
            if (!isLoad && hasError)
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height - 150,
                child: Center(
                  child: Text(
                    errorMessage,
                  ),
                ),
              ),
            if (!isLoad && hasError == false)
              Container(
                child: Column(
                  children: <Widget>[
                    HotelInfoSlider(hotelInfoPreview),
                    HotelInfoText("Страна: ",
                        hotelInfoPreview.address.country.toString()),
                    HotelInfoText(
                        "Грод: ", hotelInfoPreview.address.city.toString()),
                    HotelInfoText(
                        "Улица: ", hotelInfoPreview.address.street.toString()),
                    HotelInfoText(
                        "Рейтинг: ", hotelInfoPreview.rating.toString()),
                    HotelHeader("Сервисы"),
                    HotelServ(hotelInfoPreview.services.free,
                        hotelInfoPreview.services.paid),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
