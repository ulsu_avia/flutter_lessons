import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:hotels/models/hotel.dart';
import 'package:flutter/material.dart';
import 'package:hotels/views/list_grid.dart';
import 'package:hotels/views/list_view.dart';
import '../models/hotel.dart';

class HomeView extends StatefulWidget {
  HomeView({Key key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  bool isLoad = false;
  bool hasError = false;
  bool viewGrid = false;
  String errorMessage;
  List<HotelPreview> hotelPreview;
  Dio _dio = Dio();

  void initState() {
    super.initState();
    getDataDio();
  }

  getDataDio() async {
    setState(() {
      isLoad = true;
    });

    try {
      final response = await _dio
          .get('https://run.mocky.io/v3/ac888dc5-d193-4700-b12c-abb43e289301');
      var data = response.data;
      hotelPreview = data
          .map<HotelPreview>(
              (hotelPreview) => HotelPreview.fromJson(hotelPreview))
          .toList();
    } on DioError catch (e) {
      //print(e.response.data['message']);
      setState(() {
        errorMessage = e.response.data['message'];
        hasError = true;
        isLoad = false;
      });
    }

    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    viewGrid = false;
                  });
                },
                child: Icon(
                  Icons.list,
                  size: 26.0,
                ),
              )),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    viewGrid = true;
                  });
                },
                child: Icon(Icons.apps_sharp),
              )),
        ],
      ),
      body: Column(
        children: <Widget>[
          if (isLoad)
            Center(
              child: CircularProgressIndicator(),
            ),
          if (!isLoad && hasError) Text(errorMessage),
          if (!isLoad && hasError == false)
            if (viewGrid)
              Expanded(
                child: HotelGrid(hotelPreview: hotelPreview),
              )
            else
              Expanded(
                child: HotelList(hotelPreview: hotelPreview),
              )
        ],
      ),
    );
  }
}
