import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../models/hotel.dart';

class HotelGrid extends StatelessWidget {
  final List<HotelPreview> hotelPreview;
  HotelGrid({Key key, this.hotelPreview}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            mainAxisSpacing: 5.0,
            crossAxisSpacing: 5.0,
            crossAxisCount: 2),
        padding: EdgeInsets.only(left: 10, right: 10),
        itemCount: hotelPreview == null ? 0 : hotelPreview.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(0),
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.values,
                  children: <Widget>[
                    Container(
                      //padding: EdgeInsets.all(4),
                      height: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: const Radius.circular(10.0),
                          topLeft: const Radius.circular(10.0),
                        ),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                              'assets/images/${hotelPreview[index].poster}'),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                        //height: 80,
                        child: Text(
                          hotelPreview[index].name,
                          style: TextStyle(color: Colors.black, fontSize: 14),
                          textAlign: TextAlign.center,
                          textDirection: TextDirection.ltr,
                        ),
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () {
                          // setState(
                          //       () {
                          //     _text = myController.text;
                          //   },
                          // );
                          Navigator.of(context).pushNamed('/hotelinfo', arguments: {'uuid': hotelPreview[index].uuid,});
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            bottomRight: const Radius.circular(10.0),
                            bottomLeft: const Radius.circular(10.0),
                          ),
                        ),
                        color: Colors.blue,
                        child: Text(
                          "Подробнее",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
