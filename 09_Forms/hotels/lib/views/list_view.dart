import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../models/hotel.dart';

class HotelList extends StatelessWidget {
  final List<HotelPreview> hotelPreview;
  HotelList({Key key, this.hotelPreview}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: EdgeInsets.only(left: 10, right: 10),
        itemCount: hotelPreview == null ? 0 : hotelPreview.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      //padding: EdgeInsets.all(4),
                      height: 200,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: const Radius.circular(10.0),
                          topLeft: const Radius.circular(10.0),
                        ),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                              'assets/images/${hotelPreview[index].poster}'),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Flexible(
                            flex: 2,
                            child: Container(
                              width: 150,
                              child:
                              Text(
                                hotelPreview[index].name,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 14),
                                textAlign: TextAlign.justify,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Container(
                              //height: 100,
                              width: 200,
                              child: RaisedButton(
                                onPressed: () {
                                  // setState(
                                  //       () {
                                  //     _text = myController.text;
                                  //   },
                                  // );
                                  Navigator.of(context).pushNamed('/hotelinfo', arguments: {'uuid': hotelPreview[index].uuid,});
                                  //print("2222");
                                  //FocusScope.of(context).requestFocus(nodeTwo);
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(2.0)),
                                color: Colors.blue,
                                child: Text(
                                  "Подробнее",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
