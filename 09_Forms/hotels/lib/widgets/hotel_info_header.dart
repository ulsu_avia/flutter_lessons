import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:carousel_slider/carousel_slider.dart';

class HotelHeader extends StatelessWidget {
  HotelHeader(this.text);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20.0, bottom: 5.0, left: 4.0),
      child: Row(
        children: <Widget>[
          Text(
            text,
            style: TextStyle(color: Colors.black, fontSize: 25),
            textAlign: TextAlign.start,
            textDirection: TextDirection.ltr,
          ),
        ],
      ),
    );
  }
}
