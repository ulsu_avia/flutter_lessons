import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:hotels/models/hotel.dart';
import 'package:carousel_slider/carousel_slider.dart';

class HotelInfoSlider extends StatelessWidget {
  HotelInfoSlider(this.hotelInfoPreview);

  HotelInfoPreview hotelInfoPreview;

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
        items: List<Widget>.generate(
            hotelInfoPreview.photos.length,
            (i) => Image.asset(
                  'assets/images/${hotelInfoPreview.photos[i]}',
                  fit: BoxFit.fill,
                )),
        options: CarouselOptions(
          height: 200,
          aspectRatio: 16 / 9,
          viewportFraction: 0.8,
          initialPage: 0,
          enableInfiniteScroll: true,
          reverse: false,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 3),
          autoPlayAnimationDuration: Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,
          enlargeCenterPage: false,
          scrollDirection: Axis.horizontal,
        ));
  }
}
