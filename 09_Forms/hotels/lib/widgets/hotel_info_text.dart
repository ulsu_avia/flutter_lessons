import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:carousel_slider/carousel_slider.dart';


class HotelInfoText extends StatelessWidget {
  HotelInfoText(this.par, this.val);

  final String par;
  final String val;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4.0),
      child: Row(
        children: <Widget>[
          Text(
            par,
            style: TextStyle(color: Colors.black, fontSize: 14),
            textAlign: TextAlign.start,
            textDirection: TextDirection.ltr,
          ),
          Text(
            val,
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14),
            textAlign: TextAlign.start,
            textDirection: TextDirection.ltr,
          ),
        ],
      ),
    );
  }
}