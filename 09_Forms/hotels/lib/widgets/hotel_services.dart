import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:carousel_slider/carousel_slider.dart';

class HotelServ extends StatelessWidget {
  HotelServ(this.free, this.paid);

  final List<String> paid;
  final List<String> free;
  //final String text;

  // ListView.builder(
  //     itemCount: paid.length,
  //     itemBuilder: (BuildContext context, int index) {
  //       return Text(paid[index]);
  //     }),

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Text(
                  'Платные',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.ltr,
                ),
              ),
              Expanded(
                child: Text(
                  'Бесплатные',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.ltr,
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: paid.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Text(paid[index]);
                    }),
              ),
              Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: free.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Text(free[index]);
                    }),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
